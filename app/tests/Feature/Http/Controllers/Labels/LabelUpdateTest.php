<?php

namespace Tests\Feature\Feature\Http\Controllers\Labels;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Label;
use Tests\TestCase;

class LabelUpdateTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * Test Label update.
     */
    public function testLabelUpdate()
    {
        $this->post('/api/label',[
            "name" => "testLabelUpdate",
            "budgetID" => "2",
            "weight" => "55"
        ]);

        $testLabel = Label::latest('id')->first();

        $response = $this->put('/api/label/'.$testLabel['id'],[
            "name" => "newTestLabelUpdate",
            "weight" => "43"
        ]);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();

        $this->assertEquals($response['id'], $testLabel['id']);

        $this->assertDatabaseHas('labels',[
            "name" => "newTestLabelUpdate",
            "budgetID" => "2",
            "weight" => "43"
        ]);
    }

    /**
     * Test Label update invalid URL
    */
    public function testLabelUpdateInvalidURL()
    {
        $this->post('/api/label',[
            "name" => "testLabelUpdate",
            "budgetID" => "2",
            "weight" => "55"
        ]);

        $testLabel = Label::latest('id')->first();

        $response = $this->put('/api/labelInvalidURL/'.$testLabel['id'],[
            "name" => "newTestLabelUpdate",
            "weight" => "43"
        ]);

        $response->assertStatus(404);
    }

    /**
     * Test Label update invalid method.
     */
    public function testLabelUpdateInvalidMethod()
    {
        $this->post('/api/label',[
            "name" => "testLabelUpdate",
            "budgetID" => "2",
            "weight" => "55"
        ]);

        $testLabel = Label::latest('id')->first();

        $response = $this->post('/api/label/'.$testLabel['id'],[
            "name" => "newTestLabelUpdate",
            "weight" => "43"
        ]);

        $response->assertStatus(405);
    }

    /**
     * Test Label update with empty parameter.
    */
    public function testLabelUpdateValidationErrorEmpty()
    {
        $this->post('/api/label',[
            "name" => "testLabelUpdate",
            "budgetID" => "2",
            "weight" => "55"
        ]);
        $testLabel = Label::latest('id')->first();

        $response = $this->put('/api/label/'.$testLabel['id'],[
            "name" => "",
            "weight" => ""
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'name' => 'The name field is required.',
            'weight' => 'The weight field is required.'
        ]);
    }
        
    /**
     * Test Label update with invalid parameter type.
    */
    public function testLabelUpdateValidationInvalidType()
    {
        $this->post('/api/label',[
            "name" => "testLabelUpdate",
            "budgetID" => "2",
            "weight" => "55"
        ]);
        $testLabel = Label::latest('id')->first();

        $response = $this->put('/api/label/'.$testLabel['id'],[
            "name" => true,
            "budgetID" => "4",
            "weight" => "String"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'name' => 'The name must be a string.',
            'budgetID' => 'The budget i d field is prohibited.',
            'weight' => 'The weight must be an integer.',
        ]);
    }
        
    /**
     * Test Label update with invalid value length.
    */
    public function testLabelUpdateValidationInvalidValueLength()
    {
        $this->post('/api/label',[
            "name" => "testLabelUpdate",
            "budgetID" => "2",
            "weight" => "55"
        ]);
        $testLabel = Label::latest('id')->first();

        $response = $this->put('/api/label/'.$testLabel['id'],[
            "name" => "newTestLabelUpdate",
            "weight" => "101",
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'weight' => 'The weight must be between 1  and 100.'
        ]);
    }
}
