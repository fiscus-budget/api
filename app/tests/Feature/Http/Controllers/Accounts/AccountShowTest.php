<?php

namespace Tests\Feature\Http\Controllers\Accounts;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Account;
use Tests\TestCase;

class AccountShow extends TestCase
{
    use DatabaseTransactions;
    /**
     * Tests Account show.
     *
     */
    public function testAccountShow()
    {
        $testAccountID = Account::latest('id')->first()->id;
        $response = $this->get('/api/account/'.$testAccountID);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
    }    
    
    /**
     * Tests Account show method with invalid URL.
     *
     */
    public function testAccountShowInvalidPath()
    {
        $testAccountID = Account::latest('id')->first()->id;
        $response = $this->post('/api/accountInvalidPath/'.$testAccountID);

        $response->assertStatus(404);
    }

    /**
     * Tests Account show method with wrong method call.
     *
     */
    public function testAccountShowInvalidMethod()
    {
        $testAccountID = Account::latest('id')->first()->id;
        $response = $this->post('/api/account/'.$testAccountID);

        $response->assertStatus(405);
    }
}
