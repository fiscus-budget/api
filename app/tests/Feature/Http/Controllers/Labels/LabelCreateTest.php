<?php

namespace Tests\Feature\tests\Feature\Http\Controllers\Labels;

use App\Models\Label;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LabelCreateTest extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Test Label create.
     */
    public function testLabelCreate()
    {
        $response = $this->post('/api/label',[
            "name" => "testLabelCreate",
            "budgetID" => "2",
            "weight" => "51"
        ]);
        $testLabel = Label::latest('id')->first();

        $response->assertStatus(201);
        $response->assertSessionHasNoErrors();

        $this->assertEquals($response['id'], $testLabel['id']);

        $this->assertDatabaseHas('labels',[
            "name" => "testLabelCreate",
            "budgetID" => "2",
            "weight" => "51"
        ]);
    }

    /**
     * Test Label create with invalid URL.
     */
    public function testLabelCreateInvalidPath()
    {
        $response = $this->post('/api/labelInvalidPath',[
            "name" => "testLabelCreate",
            "budgetID" => "2",
            "weight" => "51"
        ]);

        $response->assertStatus(404);
    }

    /**
     * Test Label with invalid method.
     */
    public function testLabelCreateInvalidMethod()
    {
        $response = $this->get('/api/label',[
            "name" => "testLabelCreate",
            "budgetID" => "2",
            "weight" => "51"
        ]);

        $response->assertStatus(405);
    }

    /**
     * Test Label create with empty parameter.
    */
    public function testLabelCreateValidationErrorEmpty()
    {
        $response = $this->post('/api/label',[
            "name" => "",
            "budgetID" => "",
            "weight" => ""
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'name' => 'The name field is required.',
            'budgetID' => 'The budget i d field is required.',
            'weight' => 'The weight field is required.'
        ]);
    }
        
    /**
     * Test Label create with invalid parameter type.
    */
    public function testLabelCreateValidationInvalidType()
    {
        $response = $this->post('/api/label',[
            "name" => true,
            "budgetID" => "String",
            "weight" => "String"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'name' => 'The name must be a string.',
            'budgetID' => 'The budget i d must be an integer.',
            'weight' => 'The weight must be an integer.',
        ]);
    }
        
    /**
     * Test Label create with invalid value length.
    */
    public function testLabelCreateValidationInvalidValueLength()
    {
        $response = $this->post('/api/label',[
            "name" => "testLabelCreate",
            "budgetID" => "2",
            "weight" => "101"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'weight' => 'The weight must be between 1  and 100.'
        ]);
    }
}
