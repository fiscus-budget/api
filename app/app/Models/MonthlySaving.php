<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MonthlySaving extends Model
{
    use HasFactory;

    protected $table = 'monthly_savings';

    protected $primaryKey = 'id';
    protected $fillable = ['assignedValue','jobID', 'monthYear'];

    public function job(){
        return $this->belongsTo(Job::class, 'jobID');
    }

    public $jobID;
    public DateTime $monthDate;

    public function getExpenseValueAttribute()
    {
        $savingDate = new DateTime($this->monthYear);

        $transactions = Transaction::where('delete' ,'=', 0)->where('chargeJobID','=', $this->jobID)->whereMonth('date', $savingDate->format('m'))
        ->whereYear('date', $savingDate->format('Y'))
        ->get();

        $expenseCount = 0;
        foreach($transactions as $transaction){
            $expenseCount += $transaction->value;
        }
        return $expenseCount;
    }   

    //TODO if not used
    // public static function createMonthSet(int $jobID){
    //     $months = array();

    //     $monthCount = 120;
        
    //     $yearCount = 0;
    //     $yearsMonth = 1;

    //     for ($i = 1; $i <= $monthCount; $i++) {

    //         if( $i % 12 == 0)
    //         {
    //             ++$yearCount;
    //             $yearsMonth = 1;
    //         }

    //         $newMonth = [
    //             'jobID' => $jobID,
    //             'assignedValue' => '0',
    //             'monthYear' => 2020+$yearCount.'-'.$yearsMonth.'-01'
    //         ];
    //         array_push($months, $newMonth);
    //         ++$yearsMonth;
    //     }

    //     return $months;

    // }
}
