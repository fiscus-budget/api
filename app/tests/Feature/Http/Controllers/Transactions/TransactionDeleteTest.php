<?php

namespace Tests\Feature\Http\Controllers\Transactions;

use App\Models\Transaction;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TransactionDelete extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test Transaction delete.
     */
    public function testTransactionDelete()
    {
        $this->post('/api/transaction',[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "budgetID" => "2",
            "value" => "10"
        ]);
        $testTransaction = Transaction::latest('id')->first();

        $response = $this->delete('/api/transaction/'.$testTransaction['id']);
        $deletedTransaction = Transaction::latest('id')->first();

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
        $this->assertEquals(true, $deletedTransaction['delete']);
    }

    /**
     * Test Transaction delete invalid URL
    */
    public function testTransactionDeleteInvalidURL()
    {
        $this->post('/api/transaction',[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "budgetID" => "2",
            "value" => "10"
        ]);

        $testTransaction = Transaction::latest('id')->first();
        $response = $this->Delete('/api/transactionInvalidURL/'.$testTransaction['id']);

        $response->assertStatus(404);
    }


    /**
     * Test Transaction delete invalid method.
     */
    public function testTransactionDeleteInvalidMethod()
    {
        $this->post('/api/transaction',[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "budgetID" => "2",
            "value" => "10"
        ]);

        $testTransaction = Transaction::latest('id')->first();

        $response = $this->post('/api/transaction/'.$testTransaction['id']);

        $response->assertStatus(405);
    }
}
