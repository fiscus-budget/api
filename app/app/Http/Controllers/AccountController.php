<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAccountRequest;
use App\Http\Requests\UpdateAccountRequest;
use App\Models\Account;
use App\Helpers\ControllerHelper;
use App\Enums\DeleteEnum;
use App\GlobalConstants\OffsetLimitConstant;
use Illuminate\Http\Request;



class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $helper = new ControllerHelper; 
        $helper = new ControllerHelper;
        $offset = $helper->setQueryParameter($request->query("offset"), OffsetLimitConstant::OFFSET);
        $offset = $helper->checkIntVal($offset, OffsetLimitConstant::OFFSET);
        $limit = $helper->setQueryParameter($request->query("limit"), OffsetLimitConstant::LIMIT);
        $limit = $helper->checkIntVal($limit, OffsetLimitConstant::LIMIT);
        $delete = $helper->setQueryParameter($request->query("delete"), DeleteEnum::FALSE);
        $budgetID = $request->query('budgetID');
        
        if ($delete == DeleteEnum::ALL) {
            $indexAccounts =  Account::
            where('budgetID', $budgetID)->
            orderBy('id', 'ASC')->get();
        } else{
            $indexAccounts = Account::
            where('budgetID', $budgetID)->
            where('delete', '=', $helper->reverseCastDelete($delete))->
            orderBy('id', 'ASC')->get();
        }

        $indexAccounts = $indexAccounts->slice($offset, $limit);
        $displayAccounts = []; 

        foreach($indexAccounts as $accounts){
            array_push($displayAccounts, $accounts);
        }

        return $displayAccounts;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAccountRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAccountRequest $request)
    {
        $newAccount = Account::create([
            'budgetID' => $request->input('budgetID'),
            'delete' => DeleteEnum::FALSE,
            'name' => $request->input('name'),
            'value' => $request->input('value'),
            'typeID' => $request->input('typeID')
        ]);
        return $newAccount;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $accountID
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $accountID)
    { 
        //QUESTION delete selection when calling individual account?
        $helper = new ControllerHelper; 
        $delete = $helper->setQueryParameter($request->query("delete"), 0);

        //API change accounttype according this definition
        return Account::
        with('accountType')->
        where('delete', '=', $delete)->
        find($accountID);
        //  TODO remove if not used
        // $showAccount= Account::with('accountType')->find($accountID);

        // $type = Account::find($accountID)->accountType()
        // ->select('name')
        // ->first();


        // $showAccount = new Account([
        //     'id' => $showAccount->id,
        //     'budgetID' => $showAccount->budgetID,
        //     'delete' => $showAccount->delete,
        //     'name' => $showAccount->name,
        //     'value' => $showAccount->value,
        //     
        //     'typeID' => $type->name
        // ]);
        
        // return $showAccount;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAccountRequest  $request
     * @param  int $accountID
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAccountRequest $request, $accountID)
    {
        $updateAccount = Account::find($accountID);
        $updateAccount->update($request->all());
        return $updateAccount;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $accountID
     * @return \Illuminate\Http\Response
     */
    public function destroy($accountID)
    {
        return Account::destroy($accountID);
    }

    /**
     * Flags the specified resource as delete.
     *
     * @param  int $accountID
     * @return \Illuminate\Http\Response
     */
    public function flagDelete($accountID)
    {
        $deleteAccount = Account::find($accountID);
        $deleteAccount->update([
            'delete' => DeleteEnum::TRUE
        ]);
        return $deleteAccount;
    }
}
