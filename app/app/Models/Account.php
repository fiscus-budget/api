<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AccountType;

class Account extends Model
{
    use HasFactory;
    protected $table = 'accounts';

    protected $primaryKey = 'id';
    protected $fillable = ['id','budgetID','name', 'value','typeID', 'delete'];


    public function accountType(){
        return $this->belongsTo(AccountType::class, 'typeID');
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'delete' => 'boolean',
    ];
}
