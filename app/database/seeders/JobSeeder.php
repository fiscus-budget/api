<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Job;
use DateTime;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Job::factory()->times(20)->create();
        Job::create([
            'budgetID' => 6,
            'savingType' => 0,
            'name' => 'testJob',
            'valueGoal' => 10000,
            'dateGoal' => new DateTime('2023-01-01'),
            'delete' => false
        ]);
    }
}
