<?php

namespace App\GlobalConstants;

class OffsetLimitConstant {
    const OFFSET = 0;
    const LIMIT = 50;
}
