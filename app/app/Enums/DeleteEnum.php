<?php

namespace App\Enums;
use App\Enums\Enum;

abstract class DeleteEnum extends Enum{
    const TRUE = true;
    const FALSE = false;
    const ALL = "all";
}
