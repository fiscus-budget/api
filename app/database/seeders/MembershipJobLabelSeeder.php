<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MembershipJobLabel;

class MembershipJobLabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MembershipJobLabel::factory()->times(300)->create();
    }
}
