<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class LabelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'budgetID' => $this->faker->numberBetween($min = 1, $max = 10),
            'weight' => $this->faker->numberBetween($min = 1, $max = 100),
            'name' => $this->faker->colorName(),
        ];
    }
}
