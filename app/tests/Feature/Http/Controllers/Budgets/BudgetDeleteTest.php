<?php

namespace Tests\Feature\Http\Controllers\Budgets;
use App\Models\Budget;
use Tests\TestCase;

class BudgetDelete extends TestCase
{
    /**
     * Test Budget delete.
     */
    public function testBudgetDelete()
    {
        $newBudget = $this->post('/api/budget',[
            "name" => "testBudgetDelete"
        ]);

        $testBudget = Budget::latest('id')->first();
        $response = $this->delete('/api/budget/'.$testBudget['id']);
        $deletedBudget = Budget::latest('id')->first();

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
        $this->assertEquals(true, $deletedBudget['delete']);

        Budget::destroy($newBudget['id']);
    }

    /**
     * Test Budget delete invalid URL
    */
    public function testBudgetDeleteInvalidURL()
    {
        $newBudget = $this->post('/api/budget',[
            "name" => "testBudgetDelete"
        ]);

        $testBudget = Budget::latest('id')->first();

        $response = $this->Delete('/api/budgetInvalidURL/'.$testBudget['id']);

        $response->assertStatus(404);
        
        Budget::destroy($newBudget['id']);
    }

    /**
     * Test Budget delete invalid method.
     */
    public function testBudgetDeleteInvalidMethod()
    {
        $newBudget = $this->post('/api/budget',[
            "name" => "testBudgetDelete"
        ]);

        $testBudget = Budget::latest('id')->first();

        $response = $this->post('/api/budget/'.$testBudget['id']);

        $response->assertStatus(405);
        
        Budget::destroy($newBudget['id']);
    }
}
