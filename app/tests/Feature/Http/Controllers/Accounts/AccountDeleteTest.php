<?php

namespace Tests\Feature\Http\Controllers\Accounts;
use App\Models\Account;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AccountDelete extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test Account delete.
     */
    public function testAccountDelete()
    {
        $this->post('/api/account',[
            "name" => "testAccountDelete",
            "budgetID" => "2",
            "value" => "546",
            "typeID" => "3"
        ]);
        $testAccount = Account::latest('id')->first();

        $response = $this->delete('/api/account/'.$testAccount['id']);
        $deletedAccount = Account::latest('id')->first();

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
        $this->assertEquals(true, $deletedAccount['delete']);
    }

    /**
     * Test Account delete invalid URL
    */
    public function testAccountDeleteInvalidURL()
    {
        $this->post('/api/account',[
            "name" => "testAccountDelete",
            "budgetID" => "2",
            "value" => "546",
            "typeID" => "3"
        ]);

        $testAccount = Account::latest('id')->first();
        $response = $this->Delete('/api/accountInvalidURL/'.$testAccount['id']);

        $response->assertStatus(404);
    }


    /**
     * Test Account delete invalid method.
     */
    public function testAccountDeleteInvalidMethod()
    {
        $this->post('/api/account',[
            "name" => "testAccountDelete",
            "budgetID" => "2",
            "value" => "546",
            "typeID" => "3"
        ]);

        $testAccount = Account::latest('id')->first();

        $response = $this->post('/api/account/'.$testAccount['id']);

        $response->assertStatus(405);
    }
}