<?php

namespace Tests\Feature\Feature\Http\Controllers\Labels;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Label;
use Tests\TestCase;

class LabelShowTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * Tests Label show.
     *
     */
    public function testLabelShow()
    {
        $testLabelID = Label::latest('id')->first()->id;
        $response = $this->get('/api/label/'.$testLabelID);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
    }    
    
    /**
     * Tests Label show method with invalid URL.
     *
     */
    public function testLabelShowInvalidPath()
    {
        $testLabelID = Label::latest('id')->first()->id;
        $response = $this->post('/api/labelInvalidPath/'.$testLabelID);

        $response->assertStatus(404);
    }

    /**
     * Tests Label show method with wrong method call.
     *
     */
    public function testLabelShowInvalidMethod()
    {
        $testLabelID = Label::latest('id')->first()->id;
        $response = $this->post('/api/label/'.$testLabelID);

        $response->assertStatus(405);
    }
}
