<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [

            'budgetID' => $this->faker->numberBetween($min = 1, $max = 10),
            'date' => $this->faker->dateTimeBetween($format = 'Y-m-d', $startDate = '+50 years'),
            
            'chargeAccountID' => $this->faker->numberBetween($min = 1, $max = 100),
            'favorOfAccountID' => $this->faker->numberBetween($min = 1, $max = 100),
            'chargeJobID' => $this->faker->numberBetween($min = 1, $max = 20),

            'value' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = -200, $max = 200),

            'delete' => $this->faker->boolean($chanceOfGettingTrue = 25)
        ];
    }
}
