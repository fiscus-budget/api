<?php

namespace Tests\Feature\Http\Controllers\MonthlySavings;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\MonthlySaving;
use Tests\TestCase;

class MonthlySavingUpdate extends TestCase
{
    use DatabaseTransactions;
    /**
     * Test MonthlySaving update.
     */
    public function testMonthlySavingUpdate()
    {
        $testMonthlySaving = MonthlySaving::latest('id')->first();

        $response = $this->put('/api/job/month/'.$testMonthlySaving['jobID'].'/'.$testMonthlySaving['monthYear'],[
            'assignedValue' => '545'
        ]);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();

        $this->assertEquals($response['id'], $testMonthlySaving['id']);

        $this->assertDatabaseHas('monthly_savings',[
            "jobID" => $testMonthlySaving['jobID'],
            "monthYear" => $testMonthlySaving['monthYear'],
            'assignedValue' => '545'
        ]);
    }

    /**
     * Test MonthlySaving update with new month.
     */
    public function testMonthlySavingUpdateNewMonth()
    {
        $testMonthlySaving = MonthlySaving::latest('id')->first();

        $response = $this->put('/api/job/month/'.$testMonthlySaving['jobID'].'/2099-03-29',[
            'assignedValue' => '545'
        ]);

        $response->assertStatus(201);
        $response->assertSessionHasNoErrors();

        $newTestMonthlySaving = MonthlySaving::latest('id')->first();
        $this->assertEquals($response['id'], $newTestMonthlySaving['id']);

        $this->assertDatabaseHas('monthly_savings',[
            "jobID" => $testMonthlySaving['jobID'],
            "monthYear" => '2099-03-29',
            'assignedValue' => '545'
        ]);
    }

    /**
     * Test MonthlySaving update invalid URL
    */
    public function testMonthlySavingUpdateInvalidURL()
    {
        $testMonthlySaving = MonthlySaving::latest('id')->first();

        $response = $this->put('/api/job/monthInvalidPath/'.$testMonthlySaving['jobID'].'/'.$testMonthlySaving['monthYear'],[
            'assignedValue' => '545'
        ]);


        $response->assertStatus(404);
    }

    /**
     * Test MonthlySaving update with empty parameter.
    */
    public function testMonthlySavingUpdateValidationErrorEmpty()
    {
        $testMonthlySaving = MonthlySaving::latest('id')->first();

        $response = $this->put('/api/job/month/'.$testMonthlySaving['jobID'].'/'.$testMonthlySaving['monthYear'],[
            'assignedValue' => ''
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'assignedValue' => 'The assigned value field is required.',
        ]);
    }
        
    /**
     * Test MonthlySaving update with invalid parameter type.
    */
    public function testMonthlySavingUpdateValidationInvalidType()
    {
        $testMonthlySaving = MonthlySaving::latest('id')->first();

        $response = $this->put('/api/job/month/'.$testMonthlySaving['jobID'].'/'.$testMonthlySaving['monthYear'],[
            'assignedValue' => 'String'
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'assignedValue' => 'The assigned value must be a number.',
            'assignedValue' => 'The assigned value format is invalid.',
        ]);
    }
        
    /**
     * Test MonthlySaving update with invalid value length.
    */
    public function testMonthlySavingUpdateValidationInvalidValueLength()
    {
        $testMonthlySaving = MonthlySaving::latest('id')->first();

        $response = $this->put('/api/job/month/'.$testMonthlySaving['jobID'].'/'.$testMonthlySaving['monthYear'],[
            'assignedValue' => '-9999999999999999999.99'
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'assignedValue' => 'The assigned value must be between -999999999999999999.99 and 999999999999999999.99.'
        ]);
    }
}
