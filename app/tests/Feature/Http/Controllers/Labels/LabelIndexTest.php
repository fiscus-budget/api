<?php

namespace Tests\Feature\Feature\Http\Controllers\Labels;

use App\Models\Label;
use App\Models\Budget;
use App\GlobalConstants\OffsetLimitConstant;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LabelIndexTest extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Tests Label index.
     *
     */
    public function testLabelIndex()
    {
        $testBudgetID = Budget::find(5)->id;

        $response = $this->call('GET', '/api/labels/?budgetID='.$testBudgetID);
        $response->assertStatus(200);
    }   
    
    /**
     * Tests Label index method with wrong path.
     *
     */
    public function testLabelIndexInvalidPath()
    {
        $testBudgetID = Budget::find(5)->id;  

        $response = $this->post('/api/labelsInvalidPath/?budgetID='.$testBudgetID);

        $response->assertStatus(404);
    }

    /**
     * Tests Label index method with invalid budgetID.
     *
     */
    public function testLabelIndexInvalidBudgetID()
    {
        $invalidBudgetID = Budget::all()->Count()+1;

        $response = $this->post('/api/labels/?budgetID='.$invalidBudgetID);

        $response->assertStatus(405);
    }
        
    /**
     * Tests Label index method with budgetID not set.
     *
     */
    public function testLabelIndexBudgetIDNotSet()
    {
        $response = $this->post('/api/labels/');

        $response->assertStatus(405);
    }

    /**
     * Tests Label index method with wrong method call.
     *
     */
    public function testLabelIndexInvalidMethod()
    {
        $response = $this->post('/api/labels');

        $response->assertStatus(405);
    }

    /**
     * Tests Label index with query parameters.
     *
     */
    public function testLabelIndexQueryParameter()
    {
        $testBudgetID = Budget::find(5)->id;  
        
        $offset = 2;
        $limit = 5;

        $labelCount = Label::
        where('budgetID', $testBudgetID)
        ->get();
        $labelCount = $labelCount->slice($offset, $limit)->count(); 

        $response = $this->call('GET', '/api/labels?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit);
        
        $response->assertJsonCount($labelCount);
        $response->assertStatus(200);

    }

    /**
     * Tests Label index with zero offset and index.
     *
     */
    public function testLabelIndexZeroLimit()
    {
        $testBudgetID = Budget::find(5)->id;  

        $offset = -2;
        $limit = -4;
        
        $response = $this->call('GET', '/api/labels?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit);

        $response->assertJsonCount(0);
        $response->assertStatus(200);
    }    

    /**
     * Test Label index with invalid offset and limit.
     */
    public function testLabelIndexInvalidOffsetLimit()
    {
        $testBudgetID = Budget::find(5)->id;  
        $offset = "string";
        $limit = "false";

        $labelCount = Label::
        where('budgetID', $testBudgetID)
        ->get();
        $labelCount = $labelCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/labels?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit);

        $response->assertJsonCount($labelCount);
        $response->assertStatus(200);
    } 
}
