<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;

class Job extends Model
{
    use HasFactory;

    protected $table = 'jobs';

    protected $primaryKey = 'id';


    protected $fillable = ['budgetID',
    'savingType',
    'name',
    'valueGoal',
    'dateGoal',
    'delete'];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'delete' => 'boolean',
    ];

    public $appends = ['monthlyGoal', 'storedValue', 'month']; 

    public $specificMonth;

    /**
     * Calculates and returns the monthly goal.
     *
     * @var decimal
     */
    public function getMonthlyGoalAttribute(){

        $dateGoal = new DateTime($this->dateGoal);
        $dateGoal = new DateTime($dateGoal->format('Y-m'));

        $date = $this->specificMonth;
        if ($date === null) {
            $date = new DateTime();
        }
    
        $date = new DateTime($date->format('Y-m'));

        $monthDifference = $dateGoal->diff($date);

        $monthDifference = (($monthDifference->format('%y') * 12) + $monthDifference->format('%m'));
        
        if ($monthDifference == 0 || $dateGoal < $date) {
            $monthlyGoal = $this->valueGoal - $this->getStoredValueAttribute();
        } else {
            $monthlyGoal = ($this->valueGoal - $this->getStoredValueAttribute())/$monthDifference;
        }
        switch ($this->savingType) {
            case 0:
                return $monthlyGoal;
            case 1:
                return 0;
            case 2:
                return $this->valueGoal;
        }
    }

    /**
     * Collects and returns the monthly assigned value.
     *
     * @var decimal
     */
    public function getStoredValueAttribute(){
        $monthlySavings = MonthlySaving::where('jobID','=', $this->id)
        ->get();

        $monthlySavings = $monthlySavings->append('ExpenseValue');
              
        $expenseCount = 0;

        foreach($monthlySavings as $monthlySaving){
            $expenseCount += $monthlySaving->assignedValue;
            $monthlySaving->jobID = $this->id;
            $expenseCount += $monthlySaving->ExpenseValue;
        }

        return $expenseCount;
    }


    /**
     * Returns a specific month.
     *
     * @var decimal
     */
    public function getMonthAttribute(){
        
        $date = $this->specificMonth;

        if ($date === null) {
            $date = new DateTime();
        }

        $monthlySavings = MonthlySaving::where('jobID','=', $this->id)->whereMonth('monthYear', $date->format('m'))
        ->whereYear('monthYear', $date->format('Y'))
        ->get();
    
        if (count($monthlySavings) == 0) {
            $monthlySavings->add(new MonthlySaving([
                'jobID' => $this->id,
                'monthYear' => $date->format("Y-m-d"),
                'assignedValue' => 0
            ]));
        }

        foreach($monthlySavings as $monthlySaving){
            $monthlySaving->jobID = $this->id;
        }

        $monthlySavings = $monthlySavings->append('ExpenseValue');

        return $monthlySavings;
    }
}
