<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLabelRequest;
use App\Http\Requests\UpdateLabelRequest;
use App\Models\Label;
use App\Helpers\ControllerHelper;
use App\GlobalConstants\OffsetLimitConstant;
use Illuminate\Http\Request;


class LabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $helper = new ControllerHelper; 
        $offset = $helper->setQueryParameter($request->query("offset"), OffsetLimitConstant::OFFSET);
        $offset = $helper->checkIntVal($offset, OffsetLimitConstant::OFFSET);
        $limit = $helper->setQueryParameter($request->query("limit"), OffsetLimitConstant::LIMIT);
        $limit = $helper->checkIntVal($limit, OffsetLimitConstant::LIMIT);
        $budgetID = $request->query('budgetID');

        $indexLabels = Label::
        where('budgetID', $budgetID)->
        orderBy('id', 'ASC')->get();
    

        $indexLabels = $indexLabels->slice($offset, $limit);
        $displayLabels = []; 

        foreach($indexLabels as $label){
            array_push($displayLabels, $label);
        }

        return $displayLabels;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreLabelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLabelRequest $request)
    {
        $newLabel = Label::create([
            'budgetID' => $request->input('budgetID'),
            'name' => $request->input('name'),
            'weight' => $request->input('weight'),
        ]);
        return $newLabel;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $labelID
     * @return \Illuminate\Http\Response
     */
    public function show(int $labelID)
    {
        return Label::find($labelID);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateLabelRequest  $request
     * @param  int $labelID
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLabelRequest $request, int $labelID)
    {
        $updateLabel = Label::find($labelID);

        //TODO exclude budgetID
        $updateLabel->update($request->all());
        return $updateLabel;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $labelID
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $labelID)
    {
        return Label::destroy($labelID);
    }
}
