<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MonthlySavingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'jobID' => $this->faker->numberBetween($min = 1, $max = 20),
            'monthYear' => $this->faker->unique->date($format = 'Y-m', $startDate = '-3 years', $endDate = '+3 years').'-01',
            'assignedValue' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 500)
        ];
    }
}
