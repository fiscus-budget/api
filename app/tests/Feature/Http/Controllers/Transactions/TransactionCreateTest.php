<?php

namespace Tests\Feature\Http\Controllers\Transactions;

use App\Models\Transaction;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TransactionCreate extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Test Transaction create.
     */
    public function testTransactionCreate()
    {
        $response = $this->post('/api/transaction',[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "budgetID" => "2",
            "value" => "10"
        ]);
        $testTransaction = Transaction::latest('id')->first();

        $response->assertStatus(201);
        $response->assertSessionHasNoErrors();

        $this->assertEquals($response['id'], $testTransaction['id']);

        $this->assertDatabaseHas('transactions',[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "budgetID" => "2",
            "value" => "10"
        ]);
    }

    /**
     * Test Transaction create with invalid URL.
     */
    public function testTransactionCreateInvalidPath()
    {
        $response = $this->post('/api/transactionInvalidPath',[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "budgetID" => "2",
            "value" => "10"
        ]);

        $response->assertStatus(404);
    }

    /**
     * Test Transaction with invalid method.
     */
    public function testTransactionCreateInvalidMethod()
    {
        $response = $this->get('/api/transaction',[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "budgetID" => "2",
            "value" => "10"
        ]);

        $response->assertStatus(405);
    }

    /**
     * Test Transaction create with empty parameter.
    */
    public function testTransactionCreateValidationErrorEmpty()
    {
        $response = $this->post('/api/transaction',[
            "date" => "",
            "chargeAccountID" => "",
            "favorOfAccountID" => "",
            "chargeJobID" => "",
            "budgetID" => "",
            "value" => ""
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'date' => 'The date field is required.',
            'chargeAccountID' => 'The charge account i d field is required.',
            'favorOfAccountID' => 'The favor of account i d field is required.',
            'budgetID' => 'The budget i d field is required.',
            'value' => 'The value field is required.'
        ]);
    }
        
    /**
     * Test Transaction create with invalid parameter type.
    */
    public function testTransactionCreateValidationInvalidType()
    {
        $response = $this->post('/api/transaction',[
            "date" => true,
            "chargeAccountID" => "string",
            "favorOfAccountID" => "string",
            "chargeJobID" => "string",
            "budgetID" => "string",
            "value" => "string"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'date' => 'The date is not a valid date.',
            'date' => 'The date does not match the format Y-m-d.',
            'budgetID' => 'The budget i d must be an integer.',
            'chargeAccountID' => 'The charge account i d must be an integer.',
            'favorOfAccountID' => 'The favor of account i d must be an integer.',
            'chargeJobID' => 'The charge job i d must be an integer.',
            'value' => 'The value must be a number.',
            'value' => 'The value format is invalid.',
        ]);
    }
        
    /**
     * Test Transaction create with invalid value length.
    */
    public function testTransactionCreateValidationInvalidValueLength()
    {
        $response = $this->post('/api/transaction',[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "budgetID" => "2",
            "value" => "-9999999999999999999.99"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'value' => 'The value must be between -999999999999999999.99 and 999999999999999999.99.'
        ]);
    }
        
    /**
     * Test Transaction create with invalid id's.
    */
    public function testTransactionCreateValidationInvalidId()
    {
        $response = $this->post('/api/transaction',[
            "date" => "2012-10-30",
            "chargeAccountID" => "-5",
            "favorOfAccountID" => "-10",
            "chargeJobID" => "-15",
            "budgetID" => "-2",
            "value" => "22.50"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            "chargeAccountID" => "The selected charge account i d is invalid.",
            "favorOfAccountID" => "The selected favor of account i d is invalid.",
            "chargeJobID" => "The selected charge job i d is invalid.",
            "budgetID" => "The selected budget i d is invalid.",
        ]);
    }
    
}
