#### Create Model including Migration
php artisan make:model Budget -a --api --test --pest


#### Run db seeder
php artisan db:seed

#### Migration with seeding, first command is necessary after creating a ned seeder
php artisan migrate:refresh --seed


#### Create Controller
php artisan make:controller BudgetsController --api --model=Budget --test --pest

#### Testing
php artisan test

#### when implementing new class use this command to set paths
composer dump-autoload


