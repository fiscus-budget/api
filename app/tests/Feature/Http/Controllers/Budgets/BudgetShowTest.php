<?php

namespace Tests\Feature\Http\Controllers\Budgets;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Budget;
use Tests\TestCase;

class BudgetShow extends TestCase
{
    use DatabaseTransactions;
    /**
     * Tests Budget show.
     *
     */
    public function testBudgetShow()
    {
        $testBudgetID = Budget::latest('id')->first()->id;
        $response = $this->get('/api/budget/'.$testBudgetID);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
    }    
    
    /**
     * Tests Budget show method with invalid URL.
     *
     */
    public function testBudgetShowInvalidPath()
    {
        $testBudgetID = Budget::latest('id')->first()->id;
        $response = $this->post('/api/budgetInvalidPath/'.$testBudgetID);

        $response->assertStatus(404);
    }

    /**
     * Tests Budget show method with wrong method call.
     *
     */
    public function testBudgetShowInvalidMethod()
    {
        $testBudgetID = Budget::latest('id')->first()->id;
        $response = $this->post('/api/budget/'.$testBudgetID);

        $response->assertStatus(405);
    }
}
