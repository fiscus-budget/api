<?php

namespace Tests\Feature\Models\Transactions;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Account;
use App\Models\Job;
use App\Models\Transaction;
use App\Enums\DeleteEnum;
use Tests\TestCase;

class TransactionTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Tests Transaction transfer method.
     *
     */
    public function testTransactionTransfer()
    {
        $testChargeAccountID = 4;
        $testFavorOfAccountID = 5;
        $testChargeJobID = 6;
        $value = 300;

        $testChargeAccountValue = Account::find($testChargeAccountID)->value;
        $testFavorOfAccountValue = Account::find($testFavorOfAccountID)->value;

        $testTransaction = new Transaction;

        $testTransaction->transfer($testChargeAccountID, $testFavorOfAccountID, $testChargeJobID, $value);

        $transferedChargeAccountValue = Account::find($testChargeAccountID)->value;
        $transferedFavorOfAccountValue = Account::find($testFavorOfAccountID)->value;
        $transferedChargeJobValue = Job::find($testChargeJobID)->storedValue;

        $this->assertEquals($testChargeAccountValue, $transferedChargeAccountValue + $value);
        $this->assertEquals($testFavorOfAccountValue, $transferedFavorOfAccountValue - $value);
    }

    /**
     * Tests Transaction transfer method without jobID.
     *
     */
    public function testTransactionTransferWithoutJob()
    {
        $testChargeAccountID = 4;
        $testFavorOfAccountID = 5;
        $testChargeJobID = null;
        $value = 300;

        $testChargeAccountValue = Account::find($testChargeAccountID)->value;
        $testFavorOfAccountValue = Account::find($testFavorOfAccountID)->value;

        $testTransaction = new Transaction;

        $testTransaction->transfer($testChargeAccountID, $testFavorOfAccountID, $testChargeJobID, $value);

        $transferedChargeAccountValue = Account::find($testChargeAccountID)->value;
        $transferedFavorOfAccountValue = Account::find($testFavorOfAccountID)->value;

        $this->assertEquals($testChargeAccountValue, $transferedChargeAccountValue + $value);
        $this->assertEquals($testFavorOfAccountValue, $transferedFavorOfAccountValue - $value);
    }

    
    /**
     * Tests Transaction revertTransfer method.
     *
     */
    public function testTransactionRevertTransfer()
    {
        $testChargeAccountID = 4;
        $testFavorOfAccountID = 5;
        $testChargeJobID = 6;
        $value = 300;

        $testChargeAccountValue = Account::find($testChargeAccountID)->value;
        $testFavorOfAccountValue = Account::find($testFavorOfAccountID)->value;

        $testTransaction = Transaction::create([
            'budgetID' => 2,
            'date' => '2021-01-15',
            'delete' => DeleteEnum::FALSE,
            'chargeAccountID' => $testChargeAccountID,
            'favorOfAccountID' => $testFavorOfAccountID,
            'chargeJobID' => $testChargeJobID,
            'value' => $value,
        ]);

        $testTransaction->revertTransfer(Transaction::latest('id')->first()->id);

        $transferedChargeAccountValue = Account::find($testChargeAccountID)->value;
        $transferedFavorOfAccountValue = Account::find($testFavorOfAccountID)->value;
        $transferedChargeJobValue = Job::find($testChargeJobID)->storedValue;

        $this->assertEquals($testChargeAccountValue, $transferedChargeAccountValue - $value);
        $this->assertEquals($testFavorOfAccountValue, $transferedFavorOfAccountValue + $value);
    }

    /**
     * Tests Transaction revertTransfer method without jobID.
     *
     */
    public function testTransactionRevertTransferWithoutJob()
    {
        $testChargeAccountID = 4;
        $testFavorOfAccountID = 5;
        $testChargeJobID = null;
        $value = 300;

        $testChargeAccountValue = Account::find($testChargeAccountID)->value;
        $testFavorOfAccountValue = Account::find($testFavorOfAccountID)->value;

        $testTransaction = Transaction::create([
            'budgetID' => 2,
            'date' => '2021-01-15',
            'delete' => DeleteEnum::FALSE,
            'chargeAccountID' => $testChargeAccountID,
            'favorOfAccountID' => $testFavorOfAccountID,
            'chargeJobID' => $testChargeJobID,
            'value' => $value,
        ]);

        $testTransaction->revertTransfer(Transaction::latest('id')->first()->id);

        $transferedChargeAccountValue = Account::find($testChargeAccountID)->value;
        $transferedFavorOfAccountValue = Account::find($testFavorOfAccountID)->value;

        $this->assertEquals($testChargeAccountValue, $transferedChargeAccountValue - $value);
        $this->assertEquals($testFavorOfAccountValue, $transferedFavorOfAccountValue + $value);
    }
}
