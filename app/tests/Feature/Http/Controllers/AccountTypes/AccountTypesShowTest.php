<?php

namespace Tests\Feature\Http\Controllers\AccountTypes;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\AccountType;
use Tests\TestCase;

class AccountTypesShowTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * Tests AccountType show.
     *
     */
    public function testAccountTypeShow()
    {
        $testAccountTypeID = AccountType::latest('id')->first()->id;
        $response = $this->get('/api/accounttype/'.$testAccountTypeID);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
    }    
    
    /**
     * Tests AccountType show method with invalid URL.
     *
     */
    public function testAccountTypeShowInvalidPath()
    {
        $testAccountTypeID = AccountType::latest('id')->first()->id;
        $response = $this->post('/api/accounttypeInvalidPath/'.$testAccountTypeID);

        $response->assertStatus(404);
    }

    /**
     * Tests AccountType show method with wrong method call.
     *
     */
    public function testAccountTypeShowInvalidMethod()
    {
        $testAccountTypeID = AccountType::latest('id')->first()->id;
        $response = $this->post('/api/accounttype/'.$testAccountTypeID);

        $response->assertStatus(405);
    }
}