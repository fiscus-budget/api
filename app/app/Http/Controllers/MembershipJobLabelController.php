<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMembershipJobLabelRequest;
use App\Models\MembershipJobLabel;
use App\Helpers\ControllerHelper;
use App\GlobalConstants\OffsetLimitConstant;
use Illuminate\Http\Request;

class MembershipJobLabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $helper = new ControllerHelper; 
        $offset = $helper->setQueryParameter($request->query("offset"), OffsetLimitConstant::OFFSET);
        $offset = $helper->checkIntVal($offset, OffsetLimitConstant::OFFSET);
        $limit = $helper->setQueryParameter($request->query("limit"), OffsetLimitConstant::LIMIT);
        $limit = $helper->checkIntVal($limit, OffsetLimitConstant::LIMIT);
        $budgetID = $request->query('budgetID');

        $indexMemberships = MembershipJobLabel::
        where('budgetID', $budgetID)->
        orderBy('id', 'ASC')->get();
    

        $indexMemberships = $indexMemberships->slice($offset, $limit);
        $displayMemberships= []; 

        foreach($indexMemberships as $membership){
            array_push($displayMemberships, $membership);
        }

        return $displayMemberships;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMembershipJobLabelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMembershipJobLabelRequest $request)
    {
        //API Add budgetID
        $newMemebership = MembershipJobLabel::create([
            'budgetID' => $request->input('budgetID'),
            'jobID' => $request->input('jobID'),
            'labelID' => $request->input('labelID'),
        ]);
        return $newMemebership;
    }

    /**
     * Display the specified resource.
     *
     * @param  int membershipJobLabelID
     * @return \Illuminate\Http\Response
     */
    public function show(int $membershipJobLabelID)
    {
        return MembershipJobLabel::find($membershipJobLabelID);
    }

    /**
     * Display the specified resource.
     *
     * @param  int labelID
     * @return \Illuminate\Http\Response
     */
    public function showJobs(Request $request)
    {

        $helper = new ControllerHelper; 
        $offset = $helper->setQueryParameter($request->query("offset"), OffsetLimitConstant::OFFSET);
        $offset = $helper->checkIntVal($offset, OffsetLimitConstant::OFFSET);
        $limit = $helper->setQueryParameter($request->query("limit"), OffsetLimitConstant::LIMIT);
        $limit = $helper->checkIntVal($limit, OffsetLimitConstant::LIMIT);
        $labelID = $request->query('labelID');

        //API Remove budgetID
        //API Change to labelID
        //QUESTION Only Jobs or Membership including Job?
        $indexMemeberships = MembershipJobLabel::
        with('job')->
        where('labelID', $labelID)->
        orderBy('id', 'ASC')->get();
    
        $indexMemeberships = $indexMemeberships->slice($offset, $limit);
        return $indexMemeberships;
    }
        
    /**
     * Display the specified resource.
     *
     * @param  int labelID
     * @return \Illuminate\Http\Response
     */
    public function showLabels(Request $request)
    {

        $helper = new ControllerHelper; 
        $offset = $helper->setQueryParameter($request->query("offset"), OffsetLimitConstant::OFFSET);
        $offset = $helper->checkIntVal($offset, OffsetLimitConstant::OFFSET);
        $limit = $helper->setQueryParameter($request->query("limit"), OffsetLimitConstant::LIMIT);
        $limit = $helper->checkIntVal($limit, OffsetLimitConstant::LIMIT);
        $jobID = $request->query('jobID');

        //API Remove budgetID
        //API Change to jobID
        //QUESTION Only Jobs or Membership including Job?
        $indexMemeberships = MembershipJobLabel::
        with('label')->
        where('jobID', $jobID)->
        orderBy('id', 'ASC')->get();
    
        $indexMemeberships = $indexMemeberships->slice($offset, $limit);
        return $indexMemeberships;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int membershipJobLabelID
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $membershipJobLabelID)
    {
        return MembershipJobLabel::destroy($membershipJobLabelID);
    }
}
