<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class JobFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'budgetID' => $this->faker->numberBetween($min = 1, $max = 10),
            'savingType' => $this->faker->numberBetween($min = 0, $max = 2),
            'name' => $this->faker->word(),
            'valueGoal' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 5_000),
            'dateGoal' => $this->faker->dateTimeBetween($format = 'Y-m-d', $startDate = '+20 years', $endDate = '+30 years'),
            'delete' => $this->faker->boolean($chanceOfGettingTrue = 25)
        ];
    }
}
