<?php

namespace Tests\Feature\Http\Controllers\Jobs;

use App\Models\Job;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class JobDelete extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test Job delete.
     */
    public function testJobDelete()
    {
        $this->post('/api/job',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            'savingType' => '2',
            'valueGoal' => '5000',
            'dateGoal' => '2023-10-26',
        ]);
        $testJob = Job::latest('id')->first();

        $response = $this->delete('/api/job/'.$testJob['id']);
        $deletedJob = Job::latest('id')->first();

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
        $this->assertEquals(true, $deletedJob['delete']);

    }

    /**
     * Test Job delete invalid URL
    */
    public function testJobDeleteInvalidURL()
    {
        $this->post('/api/job',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            'savingType' => '2',
            'valueGoal' => '5000',
            'dateGoal' => '2023-10-26',
        ]);

        $testJob = Job::latest('id')->first();
        $response = $this->Delete('/api/jobInvalidURL/'.$testJob['id']);

        $response->assertStatus(404);
    }


    /**
     * Test Job delete invalid method.
     */
    public function testJobDeleteInvalidMethod()
    {
        $this->post('/api/job',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            'savingType' => '2',
            'valueGoal' => '5000',
            'dateGoal' => '2023-10-26',
        ]);

        $testJob = Job::latest('id')->first();

        $response = $this->post('/api/job/'.$testJob['id']);

        $response->assertStatus(405);
    }
}
