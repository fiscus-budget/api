<?php

namespace Tests\Feature\Http\Controllers\Budgets;

use App\Helpers\ControllerHelper;
use App\Enums\DeleteEnum;
use App\Models\Budget;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BudgetCreate extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Test Budget create.
     */
    public function testBudgetCreate()
    {
        $response = $this->post('/api/budget',[
            "name" => "testBudgetCreate"
        ]);
        $testBudget = Budget::latest('id')->first();

        $response->assertStatus(201);
        $response->assertSessionHasNoErrors();

        $this->assertEquals($response['id'], $testBudget['id']);

        $this->assertDatabaseHas('budgets',[
            'name' => 'testBudgetCreate'
        ]);

        Budget::destroy($response['id']);

    }
    
    /**
     * Test Budget create with invalid URL.
     */
    public function testBudgetCreateInvalidPath()
    {
        $response = $this->post('/api/budgetInvalidPath',[
            "name" => "Test Budget"
        ]);

        $response->assertStatus(404);
    }

    /**
     * Test Budget with invalid method.
     */
    public function testBudgetCreateInvalidMethod()
    {
        $response = $this->get('/api/budget',[
            "name" => "Test Budget"
        ]);

        $response->assertStatus(405);
    }

    /**
     * Test Budget create with invalid parameter.
    */
    public function testBudgetCreateValidationError()
    {
        $response = $this->post('/api/budget',[
            "name" => ""
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'name' => 'The name field is required.'
        ]);
    }
}
