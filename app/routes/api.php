<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BudgetController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\AccountTypeController;
use App\Http\Controllers\JobController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\LabelController;
use App\Http\Controllers\MembershipJobLabelController;
use App\Http\Controllers\MonthlySavingController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//TODO: requiered for users.
// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

/**
 * Budgets
 */
Route::get('/budgets', [BudgetController::class, 'index']);
Route::post('/budget', [BudgetController::class, 'store']);
Route::get('/budget/{budgetID}', [BudgetController::class, 'show']);
Route::put('/budget/{budgetID}', [BudgetController::class, 'update']);
Route::delete('/budget/{budgetID}', [BudgetController::class, 'flagDelete']);

/**
 * Accounts
 */
Route::get('/accounts', [AccountController::class, 'index']);
Route::post('/account', [AccountController::class, 'store']);
Route::get('/account/{accountID}', [AccountController::class, 'show']);
Route::put('/account/{accountID}', [AccountController::class, 'update']);
Route::delete('/account/{accountID}', [AccountController::class, 'flagDelete']);

/**
 * MonthlySavings
 */
Route::get('/months', [MonthlySavingController::class, 'index']);
Route::put('/job/month/{jobID}/{date}', [MonthlySavingController::class, 'update']);

/**
 * Jobs
 */
Route::get('/jobs', [JobController::class, 'index']);
Route::post('/job', [JobController::class, 'store']);
Route::put('/job/{jobID}', [JobController::class, 'update']);
Route::get('/job/{jobID}', [JobController::class, 'show']);
Route::delete('/job/{jobID}', [JobController::class, 'flagDelete']);


/**
 * Labels
 */
Route::get('/labels', [LabelController::class, 'index']);
Route::post('/label', [LabelController::class, 'store']);
Route::put('/label/{labelID}', [LabelController::class, 'update']);
Route::get('/label/{labelID}', [LabelController::class, 'show']);
Route::delete('/label/{labelID}', [LabelController::class, 'destroy']);


/**
 * Membership Job Label
 */
Route::get('/memberships/job-labels', [MembershipJobLabelController::class, 'index']);
Route::post('/memberships/job-label', [MembershipJobLabelController::class, 'store']);
//QUESTION Change to /memberships/job-label/jobs/{jobID}
Route::get('/memberships/job-label/jobs', [MembershipJobLabelController::class, 'showJobs']);
Route::get('/memberships/job-label/labels', [MembershipJobLabelController::class, 'showLabels']);
Route::get('/memberships/job-label/{membershipID}', [MembershipJobLabelController::class, 'show']);
Route::delete('/memberships/job-label/{membershipID}', [MembershipJobLabelController::class, 'destroy']);

/**
 * Accounts
 */
Route::get('/transactions', [TransactionController::class, 'index']);
Route::post('/transaction', [TransactionController::class, 'store']);
Route::get('/transaction/{transactionID}', [TransactionController::class, 'show']);
Route::put('/transaction/{transactionID}', [TransactionController::class, 'update']);
Route::delete('/transaction/{transactionID}', [TransactionController::class, 'flagDelete']);

/**
 * Account Types
 */
Route::get('/accounttypes', [AccountTypeController::class, 'index']);
Route::get('/accounttype/{accounttypeID}', [AccountTypeController::class, 'show']);
