# Fiscus API

## Using Laravel Homestead
We bundle the laravel homestead together with the api code base. It is located in the ```homestead``` directory.
It is added as a git submodule which can be initialized as follow:

```bash
git pull
git submodule update --init
```

### Prerequisites
1. [Virtualbox|https://www.virtualbox.org/]
2. [Vagrant|https://www.vagrantup.com/]
3. Vagrant nfs plugin ```vagrant plugin install vagrant-winnfsd``` (Windows tested)
4. The box ```vagrant box add laravel/homestead``` 
5. [Hostname resolution|https://laravel.com/docs/8.x/homestead#hostname-resolution]


### The first time
Enter the homestead directory and execute the init.bat or init.sh file to initialize everything
> The generated files are excluded byt the git ignore!

Example Homestead.yaml file
```yaml
---
ip: "192.168.56.56"
memory: 4096
cpus: 4
provider: virtualbox

authorize: ~/.ssh/id_rsa.pub

keys:
  - ~/.ssh/id_rsa

folders:
  - map: ../
    to: /home/vagrant/code
    type: nfs

sites:
  - map: api.fiscus.test
    to: /home/vagrant/code/public

databases:
  - homestead

features:
  - mysql: false
  - mariadb: true
  - postgresql: false
  - ohmyzsh: false
  - webdriver: false

services:
  - enabled:
    - "mariadb"
#  - disabled:
#      - "postgresql@11-main"

#ports:
#  - send: 33060 # MySQL/MariaDB
#    to: 3306
#  - send: 4040
#    to: 4040
#  - send: 54320 # PostgreSQL
#    to: 5432
#  - send: 8025 # Mailhog
#    to: 8025
#  - send: 9600
#    to: 9600
#  - send: 27017
#    to: 27017

```

### Updating
> We cover updating when we face this issue the first time.
