<?php

namespace Tests\Feature\Http\Controllers\Transactions;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Transaction;
use Tests\TestCase;

class TransactionUpdate extends TestCase
{
    use DatabaseTransactions;
    /**
     * Test Transaction update.
     */
    public function testTransactionUpdate()
    {
        $this->post('/api/transaction',[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "budgetID" => "2",
            "value" => "10"
        ]);

        $testTransaction = Transaction::latest('id')->first();

        $response = $this->put('/api/transaction/'.$testTransaction['id'],[
            "date" => "2015-08-22",
            "chargeAccountID" => "7",
            "favorOfAccountID" => "8",
            "chargeJobID" => "",
            "value" => "100"
        ]);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();

        $this->assertEquals($response['id'], $testTransaction['id']);

        $this->assertDatabaseHas('transactions',[
            "date" => "2015-08-22",
            "chargeAccountID" => "7",
            "favorOfAccountID" => "8",
            "chargeJobID" => null,
            "value" => "100"
        ]);
    }

    /**
     * Test Transaction update invalid URL
    */
    public function testTransactionUpdateInvalidURL()
    {
        $this->post('/api/transaction',[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "budgetID" => "2",
            "value" => "10"
        ]);

        $testTransaction = Transaction::latest('id')->first();

        $response = $this->put('/api/transactionInvalidURL/'.$testTransaction['id'],[
            "date" => "2015-08-22",
            "chargeAccountID" => "7",
            "favorOfAccountID" => "8",
            "chargeJobID" => "",
            "value" => "100"
        ]);

        $response->assertStatus(404);
    }

    /**
     * Test Transaction update invalid method.
     */
    public function testTransactionUpdateInvalidMethod()
    {
        $this->post('/api/transaction',[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "budgetID" => "2",
            "value" => "10"
        ]);

        $testTransaction = Transaction::latest('id')->first();

        $response = $this->post('/api/transaction/'.$testTransaction['id'],[
            "date" => "2015-08-22",
            "chargeAccountID" => "7",
            "favorOfAccountID" => "8",
            "chargeJobID" => "",
            "value" => "100"
        ]);

        $response->assertStatus(405);
    }

    /**
     * Test Transaction update with empty parameter.
    */
    public function testTransactionUpdateValidationErrorEmpty()
    {
        $this->post('/api/transaction',[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "budgetID" => "2",
            "value" => "10"
        ]);
        $testTransaction = Transaction::latest('id')->first();

        $response = $this->put('/api/transaction/'.$testTransaction['id'],[
            "date" => "",
            "chargeAccountID" => "",
            "favorOfAccountID" => "",
            "chargeJobID" => "",
            "value" => ""
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'date' => 'The date field is required.',
            'chargeAccountID' => 'The charge account i d field is required.',
            'favorOfAccountID' => 'The favor of account i d field is required.',
            'value' => 'The value field is required.'
        ]);
    }
        
    /**
     * Test Transaction update with invalid parameter type.
    */
    public function testTransactionUpdateValidationInvalidType()
    {
        $this->post('/api/transaction',[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "budgetID" => "2",
            "value" => "10"
        ]);
        $testTransaction = Transaction::latest('id')->first();

        $response = $this->put('/api/transaction/'.$testTransaction['id'],[
            "date" => true,
            "chargeAccountID" => "string",
            "favorOfAccountID" => "string",
            "chargeJobID" => "string",
            "budgetID" => "2",
            "value" => "string"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'date' => 'The date is not a valid date.',
            'date' => 'The date does not match the format Y-m-d.',
            'budgetID' => 'The budget i d field is prohibited.',
            'chargeAccountID' => 'The charge account i d must be an integer.',
            'favorOfAccountID' => 'The favor of account i d must be an integer.',
            'value' => 'The value must be a number.',
            'value' => 'The value format is invalid.',
        ]);
    }
        
    /**
     * Test Transaction update with invalid value length.
    */
    public function testTransactionUpdateValidationInvalidValueLength()
    {
        $this->post('/api/transaction',[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "budgetID" => "2",
            "value" => "10"
        ]);
        $testTransaction = Transaction::latest('id')->first();

        $response = $this->put('/api/transaction/'.$testTransaction['id'],[
            "date" => "2012-10-30",
            "chargeAccountID" => "5",
            "favorOfAccountID" => "10",
            "chargeJobID" => "15",
            "value" => "-9999999999999999999.99"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'value' => 'The value must be between -999999999999999999.99 and 999999999999999999.99.'
        ]);
    }

    /**
     * Test Transaction update with invalid id's.
    */
    public function testTransactionUpdateValidationInvalidId()
    {
        $response = $this->post('/api/transaction',[
            "date" => "2012-10-30",
            "chargeAccountID" => "3",
            "favorOfAccountID" => "1",
            "chargeJobID" => "3",
            "budgetID" => '3',
            "value" => "22.50"
        ]);

        $testTransaction = Transaction::latest('id')->first();

        $response = $this->put('/api/transaction/'.$testTransaction['id'],[
            "date" => "2012-10-30",
            "chargeAccountID" => "-5",
            "favorOfAccountID" => "-10",
            "chargeJobID" => "-15",
            "value" => "22.50"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            "chargeAccountID" => "The selected charge account i d is invalid.",
            "favorOfAccountID" => "The selected favor of account i d is invalid.",
            "chargeJobID" => "The selected charge job i d is invalid.",
        ]);
    }
}
