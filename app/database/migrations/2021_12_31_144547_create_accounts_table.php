<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('budgetID')->nullable("false");
            $table->unsignedInteger('typeID')->nullable("false");
            $table->string('name');
            $table->double('value');
            $table->boolean('delete');
            $table->timestamps();
            $table->foreign('typeID')
            ->references('id')
            ->on('account_types')
            ->onDelete('set null');
            $table->foreign('budgetID')
            ->references('id')
            ->on('budgets')
            ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('accounts');
    }
}
