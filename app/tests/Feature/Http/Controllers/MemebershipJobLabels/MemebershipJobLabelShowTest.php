<?php

namespace Tests\Feature\Http\Controllers\MemebershipJobLabels;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\MembershipJobLabel;
use Tests\TestCase;

class MembershipJobLabelShow extends TestCase
{
    use DatabaseTransactions;
    /**
     * Tests MembershipJobLabel show.
     *
     */
    public function testMembershipJobLabelShow()
    {
        $testmembershipID = MembershipJobLabel::latest('id')->first()->id;
        $response = $this->get('/api/memberships/job-label/'.$testmembershipID);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
    }    
    
    /**
     * Tests MembershipJobLabel show method with invalid URL.
     *
     */
    public function testMembershipJobLabelShowInvalidPath()
    {
        $testmembershipID = MembershipJobLabel::latest('id')->first()->id;
        $response = $this->post('/api/memberships/job-labelInvalidPath/'.$testmembershipID);

        $response->assertStatus(404);
    }

    /**
     * Tests MembershipJobLabel show method with wrong method call.
     *
     */
    public function testMembershipJobLabelShowInvalidMethod()
    {
        $testmembershipID = MembershipJobLabel::latest('id')->first()->id;
        $response = $this->post('/api/memberships/job-label/'.$testmembershipID);

        $response->assertStatus(405);
    }
}
