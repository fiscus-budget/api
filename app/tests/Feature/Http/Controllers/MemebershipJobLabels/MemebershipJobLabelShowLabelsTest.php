<?php

namespace Tests\Feature\Http\Controllers\MemebershipJobLabels;

use App\Helpers\ControllerHelper;
use App\Enums\DeleteEnum;
use App\Models\MembershipJobLabel;
use App\Models\Job;
use App\GlobalConstants\OffsetLimitConstant;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class MemebershipJobLabelShowLabels extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Tests MembershipJobLabel showJobs.
     *
     */
    public function testMemebershipJobLabelShowLabels()
    {
        $testJobID = Job::find(5)->id;

        $membershipCount = MembershipJobLabel::
        where('jobID', $testJobID)
        ->get();
        $membershipCount = $membershipCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/memberships/job-label/labels?jobID='.$testJobID);

        $response->assertStatus(200);
        $response->assertJsonCount($membershipCount);
    }   
    
    /**
     * Tests MembershipJobLabel showJobs method with wrong path.
     *
     */
    public function testMemebershipJobLabelShowLabelsInvalidPath()
    {
        $testJobID = MembershipJobLabel::find(5)->id;  

        $response = $this->call('GET', '/api/memberships/job-labelInvalidPath/labels?jobID='.$testJobID);

        $response->assertStatus(404);
    }

    /**
     * Tests MembershipJobLabel showJobs method with wrong method call.
     *
     */
    public function testMemebershipJobLabelShowLabelsInvalidMethod()
    {
        $response = $this->post('/api/memberships/job-label/labels');

        $response->assertStatus(405);
    }

    
    /**
     * Tests MembershipJobLabel index method with invalid jobID.
     *
     */
    public function testMembershipJobLabelIndexInvalidJobID()
    {
        $invalidJobID = Job::all()->Count()+1;

        $response = $this->post('/api/memberships/job-labels/?budgetID='.$invalidJobID);

        $response->assertStatus(405);
    }
        
    /**
     * Tests MembershipJobLabel index method with jobID not set.
     *
     */
    public function testMembershipJobLabelIndexJobIDNotSet()
    {
        $response = $this->post('/api/memberships/job-labels/');

        $response->assertStatus(405);
    }

    /**
     * Tests MembershipJobLabel showJobs with query parameters.
     *
     */
    public function testMemebershipJobLabelShowLabelsQueryParameter()
    {
        $testJobID = MembershipJobLabel::find(5)->id;  
        
        $offset = 2;
        $limit = 6;

        $membershipCount = MembershipJobLabel::
        where('jobID', $testJobID)
        ->get();
        $membershipCount = $membershipCount->slice($offset, $limit)->count(); 

        $response = $this->call('GET', '/api/memberships/job-label/labels?jobID='.$testJobID.'&offset='.$offset.'&limit='.$limit);
        $response->assertJsonCount($membershipCount);
        $response->assertStatus(200);

    }

    /**
     * Tests MembershipJobLabel showJobs with zero offset and index.
     *
     */
    public function testMemebershipJobLabelShowLabelsIndexZeroLimit()
    {
        $testJobID = MembershipJobLabel::find(5)->id;  

        $offset = -2;
        $limit = -4;
        $delete = DeleteEnum::ALL;
        
        $response = $this->call('GET', '/api/memberships/job-label/labels?jobID='.$testJobID.'&offset='.$offset.'&limit='.$limit);

        $response->assertJsonCount(0);
        $response->assertStatus(200);
    }    

    /**
     * Test MembershipJobLabel showJobs with invalid offset and limit.
     */
    public function testMemebershipJobLabelShowLabelsInvalidOffsetLimit()
    {
        $testJobID = MembershipJobLabel::find(5)->id;  
        $offset = "string";
        $limit = "false";

        $membershipCount = MembershipJobLabel::
        where('jobID', $testJobID)
        ->get();
        $membershipCount = $membershipCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/memberships/job-label/labels?jobID='.$testJobID.'&offset='.$offset.'&limit='.$limit);

        $response->assertJsonCount($membershipCount);
        $response->assertStatus(200);
    }  
}
