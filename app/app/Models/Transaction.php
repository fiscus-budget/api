<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $table = 'transactions';

    protected $primaryKey = 'id';


    protected $fillable = ['budgetID','date' ,'chargeAccountID','favorOfAccountID','chargeJobID','favorOfJobID','value','description', 'delete'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'delete' => 'boolean',
    ];

    public function chargeAccount(){
        return $this->belongsTo(Account::class, 'chargeAccountID');
    }
    public function favorOfAccount(){
        return $this->belongsTo(Account::class, 'favorOfAccountID');
    }
    public function chargeJob(){
        return $this->belongsTo(Job::class, 'chargeJobID');
    }


    public function transfer ($chargeAccountId, $favorOfAccountId, $chargeJobId, int $value){
        $chargeAccount = Account::find($chargeAccountId);
        $favorOfAccount = Account::find($favorOfAccountId);

        //TODO Fix scalability bottleneck.
        // $chargeJob = Job::find($chargeJobId);

        $this->negativeCharge($chargeAccount, $value);
        $this->positiveCharge($favorOfAccount, $value);

        // if (!empty($chargeJob)) {
        //     $this->negativeCharge($chargeJob, $value);
        // }
    }
    
    public function revertTransfer ($transactionID){
        $transaction = Transaction::find($transactionID);
        $this->positiveCharge(Account::find($transaction->chargeAccountID), $transaction->value);
        $this->negativeCharge(Account::find($transaction->favorOfAccountID), $transaction->value);

        //TODO Fix scalability bottleneck.
        // if(!empty($transaction->chargeJobID)){
        //     $this->positiveCharge(Job::find($transaction->chargeJobID), $transaction->value);
        // }
    }


    private function positiveCharge ($chargeTable, $updateValue){
        $updateTable = $chargeTable->value;

        $chargeTable->update(
            [
            'value' => $updateTable + $updateValue
            ]
        );
    }
    
    private function negativeCharge ($chargeTable, $updateValue){
        $updateTable = $chargeTable->value;

        $chargeTable->update(
            [
            'value' => $updateTable - $updateValue
            ]
        );
    }
}

