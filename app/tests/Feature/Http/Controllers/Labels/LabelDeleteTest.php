<?php

namespace Tests\Feature\Feature\Http\Controllers\Labels;

use App\Models\Label;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LabelDeleteTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test Label delete.
     */
    public function testLabelDelete()
    {
        $this->post('/api/label',[
            "name" => "testLabelDelete",
            "budgetID" => "2",
            "weight" => "55"
        ]);
        $testLabel = Label::latest('id')->first();

        $response = $this->delete('/api/label/'.$testLabel['id']);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseMissing('labels',[
            "name" => "testLabelDelete",
            "budgetID" => "2",
            "weight" => "55"
        ]);

    }

    /**
     * Test Label delete invalid URL
    */
    public function testLabelDeleteInvalidURL()
    {
        $this->post('/api/label',[
            "name" => "testLabelDelete",
            "budgetID" => "2",
            "weight" => "55"
        ]);

        $testLabel = Label::latest('id')->first();
        $response = $this->Delete('/api/labelInvalidURL/'.$testLabel['id']);

        $response->assertStatus(404);
    }


    /**
     * Test Label delete invalid method.
     */
    public function testLabelDeleteInvalidMethod()
    {
        $this->post('/api/label',[
            "name" => "testLabelDelete",
            "budgetID" => "2",
            "weight" => "55"
        ]);

        $testLabel = Label::latest('id')->first();

        $response = $this->post('/api/label/'.$testLabel['id']);

        $response->assertStatus(405);
    }
}
