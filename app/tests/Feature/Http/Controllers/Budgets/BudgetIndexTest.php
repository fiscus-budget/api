<?php

namespace Tests\Feature\Http\Controllers\Budgets;
use App\Helpers\ControllerHelper;
use App\Enums\DeleteEnum;
use App\Models\Budget;
use App\GlobalConstants\OffsetLimitConstant;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class BudgetIndex extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Tests Budget index.
     *
     */
    public function testBudgetIndex()
    {
        $response = $this->call('GET', '/api/budgets');
        $response->assertStatus(200);
    }   
    
    /**
     * Tests Budget index method with wrong path.
     *
     */
    public function testBudgetIndexInvalidPath()
    {
        $response = $this->post('/api/budgetsInvalidPath');

        $response->assertStatus(404);
    }

    /**
     * Tests Budget index method with wrong method call.
     *
     */
    public function testBudgetIndexInvalidMethod()
    {
        $response = $this->post('/api/budgets');

        $response->assertStatus(405);
    }

    /**
     * Tests Budget index with query parameters.
     *
     */
    public function testBudgetIndexQueryParameter()
    {
        /**
         * Important note on testing:
         * https://stackoverflow.com/questions/42350138/how-to-seed-database-migrations-for-laravel-tests
         */
        
        $offset = 2;
        $limit = 7;
        $delete = DeleteEnum::ALL;

        $budgetCount = Budget::
        get();

        $budgetCount = $budgetCount->slice($offset, $limit)->count();

        $response = $this->call('GET', '/api/budgets?offset='.$offset.'&limit='.$limit.'&delete='.$delete);
        $response->assertJsonCount($budgetCount);
        $response->assertStatus(200);

    }

    /**
     * Tests Budget index with zero offset and index.
     *
     */
    public function testBudgetIndexZeroLimit()
    {
        $offset = -2;
        $limit = -4;
        $delete = DeleteEnum::ALL;
        
        $response = $this->call('GET', '/api/budgets?offset='.$offset.'&limit='.$limit.'&delete='.$delete);

        $response->assertJsonCount(0);
        $response->assertStatus(200);
    }    

    /**
     * Test Budget index with invalid offset and limit.
     */
    public function testBudgetIndexInvalidOffsetLimit()
    {
        $offset = "string";
        $limit = "false";
        $delete = DeleteEnum::ALL;

        $budgetCount = Budget::get();
        $budgetCount = $budgetCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/budgets?offset='.$offset.'&limit='.$limit.'&delete='.$delete);

        $response->assertJsonCount($budgetCount);
        $response->assertStatus(200);
    }  

    /**
     * Test Budget index with delete = all.
    */
    public function testBudgetIndexDeleteEqualsAll()
    {
        $delete = DeleteEnum::ALL;
        
        $response = $this->call('GET', '/api/budgets?delete='.$delete);
  
        $response->assertJsonCount(Budget::count());
        $response->assertStatus(200);
    }  
    /**
     * Test Budget index with delete = true.
     */
    public function testBudgetIndexDeleteEqualsTrue()
    {
        
        $helper = new ControllerHelper; 
        $delete = DeleteEnum::TRUE;
        
        $response = $this->call('GET', '/api/budgets?delete='.$delete);

        $response->assertJsonCount(Budget::where('delete','=',$helper->reverseCastDelete($delete))->count());
        $response->assertStatus(200);
    }  

    /**
     * Test Budget index with delete = false;
    */
    public function testBudgetIndexDeleteEqualsFalse()
    {
        
        $helper = new ControllerHelper; 
        $delete = DeleteEnum::FALSE;
        
        $response = $this->call('GET', '/api/budgets?delete='.$delete);

        $response->assertJsonCount(Budget::where('delete','=',$helper->reverseCastDelete($delete))->count());
        $response->assertStatus(200);
    }  

    /**
     * Test Budget index with invalid delete;
     */
    public function testBudgetIndexInvalidDelete()
    {
        
        $helper = new ControllerHelper; 
        $delete = "invalidDelete";
        $standardDelete= DeleteEnum::FALSE;
        
        $response = $this->call('GET', '/api/budgets?delete='.$delete);

        $response->assertJsonCount(Budget::where('delete','=',$helper->reverseCastDelete($standardDelete))->count());
        $response->assertStatus(200);
    }  
}
