<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembershipJobLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_job_labels', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('budgetID')->nullable("false");
            $table->unsignedInteger('jobID')->nullable("false");
            $table->unsignedInteger('labelID')->nullable("false");
            $table->timestamps();

            $table->foreign('budgetID')
            ->references('id')
            ->on('budgets')
            ->onDelete('set null'); 

            $table->foreign('jobID')
            ->references('id')
            ->on('jobs')
            ->onDelete('set null'); 

            $table->foreign('labelID')
            ->references('id')
            ->on('labels')
            ->onDelete('set null'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_job_labels');
    }
}
