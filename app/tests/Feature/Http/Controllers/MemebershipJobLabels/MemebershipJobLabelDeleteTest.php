<?php

namespace Tests\Feature\Http\Controllers\MemebershipJobLabels;
use App\Models\MembershipJobLabel;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class MembershipJobLabelDelete extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test MembershipJobLabel delete.
     */
    public function testMembershipJobLabelDelete()
    {
        $this->post('/api/memberships/job-label',[
            "budgetID" => "2",
            "jobID" => "17",
            "labelID" => "3"
        ]);
        $testLabel = MembershipJobLabel::latest('id')->first();

        $response = $this->delete('/api/memberships/job-label/'.$testLabel['id']);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
        $this->assertDatabaseMissing('membership_job_labels',[
            "budgetID" => "2",
            "jobID" => "17",
            "labelID" => "3"
        ]);
    }


    /**
     * Test MembershipJobLabel delete invalid URL
    */
    public function testMembershipJobLabelDeleteInvalidURL()
    {
        $this->post('/api/memberships/job-label',[
            "budgetID" => "2",
            "jobID" => "17",
            "labelID" => "3"
        ]);

        $testMembership = MembershipJobLabel::latest('id')->first();
        $response = $this->Delete('/api/memberships/job-labelInvalidURL/'.$testMembership['id']);

        $response->assertStatus(404);
    }


    /**
     * Test MembershipJobLabel delete invalid method.
     */
    public function testMembershipJobLabelDeleteInvalidMethod()
    {
        $this->post('/api/memberships/job-label',[
            "budgetID" => "2",
            "jobID" => "17",
            "labelID" => "3"
        ]);

        $testMembership = MembershipJobLabel::latest('id')->first();

        $response = $this->post('/api/memberships/job-label/'.$testMembership['id']);

        $response->assertStatus(405);
    }
}