<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreJobRequest;
use App\Http\Requests\UpdateJobRequest;
use App\Models\Job;
use App\Helpers\ControllerHelper;
use App\Enums\DeleteEnum;
use App\GlobalConstants\OffsetLimitConstant;
use DateTime;
use Illuminate\Http\Request;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $helper = new ControllerHelper; 

        
        

        $offset = $helper->setQueryParameter($request->query("offset"), OffsetLimitConstant::OFFSET);
        $offset = $helper->checkIntVal($offset, OffsetLimitConstant::OFFSET);
        $limit = $helper->setQueryParameter($request->query("limit"), OffsetLimitConstant::LIMIT);
        $limit = $helper->checkIntVal($limit, OffsetLimitConstant::LIMIT);
        $delete = $helper->setQueryParameter($request->query("delete"), DeleteEnum::FALSE);

        $budgetID = $request->query('budgetID');

        if ($delete == DeleteEnum::ALL) {
            $indexJobs =  Job::
            where('budgetID', $budgetID)->
            orderBy('id', 'ASC')->get();
        } else{
            $indexJobs = Job::
            where('budgetID', $budgetID)->
            where('delete', '=', $helper->reverseCastDelete($delete))->
            orderBy('id', 'ASC')->get();
        }

        foreach($indexJobs as $indexJob){
            $indexJob->specificMonth = $helper->setQueryParameter(new DateTime($request->query("month")), new DateTime());
        }

        $indexJobs = $indexJobs->slice($offset, $limit);
        $displayJobs = []; 

        foreach($indexJobs as $job){
            array_push($displayJobs, $job);
        }

        return $displayJobs;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreJobRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreJobRequest $request)
    {
        $newJob = Job::create([
            'budgetID' => $request->input('budgetID'),
            'delete' => DeleteEnum::FALSE,
            'name' => $request->input('name'),
            'savingType' => $request->input('savingType'),
            'valueGoal' => $request->input('valueGoal'),
            'dateGoal' => $request->input('dateGoal'),
        ]);
        return $newJob;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $jobID
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $jobID)
    {
        $helper = new ControllerHelper; 

        $showJob = Job::find($jobID);
        $showJob->specificMonth = $helper->setQueryParameter(new DateTime($request->query("month")), new DateTime());

        return $showJob;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateJobRequest  $request
     * @param  int $jobID
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateJobRequest $request, $jobID)
    {
        $updateJob = Job::find($jobID);
        $updateJob->update($request->all());

        return $updateJob;
    }

    /**
     * Remove the specified resource from storage.
     * @param  int $jobID
     * @return \Illuminate\Http\Response
     */
    public function destroy($jobID)
    {
        return Job::destroy($jobID);
    }

     /**
     * Flags the specified resource as delete.
     *
     * @param  int $jobID
     * @return \Illuminate\Http\Response
     */
    public function flagDelete($jobID)
    {
        //TODO transaction to budget pot->not necessary, independent calculation in budget where delete flaged are not calculated.
        $deleteJob = Job::find($jobID);
        $deleteJob->update([
            'delete' => DeleteEnum::TRUE
        ]);
        return $deleteJob;
    }
}
