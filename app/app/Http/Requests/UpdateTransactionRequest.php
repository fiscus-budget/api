<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\AccountExists;
use App\Rules\JobExists;

class UpdateTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date|date_format:Y-m-d',
            'budgetID' => 'prohibited',
            'chargeAccountID' => 'required|integer|exists:accounts,id',
            'favorOfAccountID' => 'required|integer|exists:accounts,id',
            'chargeJobID' => ['integer','nullable', new JobExists],
            'value' => 'required|numeric|between:-999999999999999999.99,999999999999999999.99|regex:/^[-]?[0-9]*.[0-9]?[0-9]?$/',
        ];
    }
}
