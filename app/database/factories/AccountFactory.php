<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AccountFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'budgetID' => $this->faker->numberBetween($min = 1, $max = 10),
            'typeID' => $this->faker->numberBetween($min = 1, $max = 5),
            'name' => $this->faker->company(),
            'value' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = -50_000, $max = 1_000_000),
            'delete' => $this->faker->boolean($chanceOfGettingTrue = 25)
        ];
    }
}
