<?php

namespace Tests\Feature\Http\Controllers\Jobs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Job;
use Tests\TestCase;

class JobShow extends TestCase
{
    use DatabaseTransactions;
    /**
     * Tests Job show.
     *
     */
    public function testJobShow()
    {
        $testJobID = Job::latest('id')->first()->id;
        $response = $this->get('/api/job/'.$testJobID);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
    }    
    
    /**
     * Tests Job show method with invalid URL.
     *
     */
    public function testJobShowInvalidPath()
    {
        $testJobID = Job::latest('id')->first()->id;
        $response = $this->post('/api/jobInvalidPath/'.$testJobID);

        $response->assertStatus(404);
    }

    /**
     * Tests Job show method with wrong method call.
     *
     */
    public function testJobShowInvalidMethod()
    {
        $testJobID = Job::latest('id')->first()->id;
        $response = $this->post('/api/job/'.$testJobID);

        $response->assertStatus(405);
    }
}
