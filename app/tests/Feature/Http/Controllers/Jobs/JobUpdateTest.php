<?php

namespace Tests\Feature\Http\Controllers\Jobs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Job;
use Tests\TestCase;

class JobUpdate extends TestCase
{
    use DatabaseTransactions;
    /**
     * Test Job update.
     */
    public function testJobUpdate()
    {
        $this->post('/api/job',[
            "name" => "newTestJobUpdate",
            "budgetID" => "2",
            'savingType' => '2',
            'valueGoal' => '5000',
            'dateGoal' => '2023-10-26',
        ]);

        $testJob = Job::latest('id')->first();

        $response = $this->put('/api/job/'.$testJob['id'],[
            "name" => "updatedTestJobUpdate",
            'savingType' => '1',
            'valueGoal' => '2500',
            'dateGoal' => '2023-09-26',
        ]);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();

        $this->assertEquals($response['id'], $testJob['id']);

        $this->assertDatabaseHas('jobs',[
            "name" => "updatedTestJobUpdate",
            "budgetID" => "2",
            'savingType' => '1',
            'valueGoal' => '2500',
            'dateGoal' => '2023-09-26',
        ]);
    }

    /**
     * Test Job update invalid URL
    */
    public function testJobUpdateInvalidURL()
    {
        $this->post('/api/job',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            'savingType' => '2',
            'valueGoal' => '5000',
            'dateGoal' => '2023-10-26',
        ]);

        $testJob = Job::latest('id')->first();

        $response = $this->put('/api/jobInvalidURL/'.$testJob['id'],[
            "name" => "updatedTestJobUpdate",
            "budgetID" => "2",
            'savingType' => '1',
            'valueGoal' => '2500',
            'dateGoal' => '2023-09-26',
        ]);

        $response->assertStatus(404);
    }

    /**
     * Test Job update invalid method.
     */
    public function testJobUpdateInvalidMethod()
    {
        $this->post('/api/job',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            'savingType' => '2',
            'valueGoal' => '5000',
            'dateGoal' => '2023-10-26',
        ]);

        $testJob = Job::latest('id')->first();

        $response = $this->post('/api/job/'.$testJob['id'],[
            "name" => "newTestJobUpdate",
            "value" => "544",            
            "defaultLabel" => "3",
            "defaultLabelWeight" => "2"
        ]);

        $response->assertStatus(405);
    }

    /**
     * Test Job update with empty parameter.
    */
    public function testJobUpdateValidationErrorEmpty()
    {
        $this->post('/api/job',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            'savingType' => '2',
            'valueGoal' => '5000',
            'dateGoal' => '2023-10-26',
        ]);
        $testJob = Job::latest('id')->first();

        $response = $this->put('/api/job/'.$testJob['id'],[
            "name" => "",
            'savingType' => '',
            'valueGoal' => '',
            'dateGoal' => '',
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'name' => 'The name field is required.',
            'savingType' => 'The saving type field is required.',
            'valueGoal' => 'The value goal field is required.',
        ]);
    }
        
    /**
     * Test Job update with invalid parameter type.
    */
    public function testJobUpdateValidationInvalidType()
    {
        $this->post('/api/job',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            'savingType' => '2',
            'valueGoal' => '5000',
            'dateGoal' => '2023-10-26',
        ]);
        $testJob = Job::latest('id')->first();

        $response = $this->put('/api/job/'.$testJob['id'],[
            "name" => true,
            "budgetID" => "3",
            'savingType' => 'String',
            'valueGoal' => 'String',
            'dateGoal' => 'String',
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'name' => 'The name must be a string.',
            'budgetID' => 'The budget i d field is prohibited.',
            "valueGoal" => "The value goal must be a number.",
            "valueGoal" => "The value goal format is invalid.",
            "dateGoal"=> "The date goal is not a valid date.",
            "dateGoal"=> "The date goal does not match the format Y-m-d.",
            "savingType"=> "The saving type must be an integer.",
            "savingType"=> "The saving type must not be greater than 2."
         ]);
    }
        
    /**
     * Test Job update with invalid value length.
    */
    public function testJobUpdateValidationInvalidValueLength()
    {
        $this->post('/api/job',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            'savingType' => '2',
            'valueGoal' => '5000',
            'dateGoal' => '2023-10-26',
        ]);
        $testJob = Job::latest('id')->first();

        $response = $this->put('/api/job/'.$testJob['id'],[
            "name" => "testJobCreate",
            "valueGoal" => "-9999999999999999999.99",
            "savingType" => "4",
            "dateGoal" => "2023-10-26"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'valueGoal' => 'The value goal must be between -999999999999999999.99 and 999999999999999999.99.',
            'savingType' => 'The saving type must not be greater than 2.'
        ]);
    }

    /**
     * Test Job update with savingType OnDate.
    */
    public function testJobUpdateValidationSavingTypeOnDate()
    {

        $this->post('/api/job',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            'savingType' => '2',
            'valueGoal' => '5000',
            'dateGoal' => '2023-10-26',
        ]);
        $testJob = Job::latest('id')->first();

        $response = $this->put('/api/job/'.$testJob['id'],[
            "name" => "testJobUpdate",
            "valueGoal" => "324",
            "savingType" => "0",
            "dateGoal" => ""
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'dateGoal' => 'The date goal is required.'
        ]);
    }
}
