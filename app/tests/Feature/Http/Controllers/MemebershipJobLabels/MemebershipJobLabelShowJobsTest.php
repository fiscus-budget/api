<?php

namespace Tests\Feature\Http\Controllers\MemebershipJobLabels;

use App\Helpers\ControllerHelper;
use App\Enums\DeleteEnum;
use App\Models\MembershipJobLabel;
use App\Models\Label;
use App\GlobalConstants\OffsetLimitConstant;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class MemebershipJobLabelShowJobs extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Tests MembershipJobLabel showJobs.
     *
     */
    public function testMemebershipJobLabelShowJobs()
    {
        $testLabelID = Label::find(5)->id;

        $membershipCount = MembershipJobLabel::
        where('labelID', $testLabelID)
        ->get();
        $membershipCount = $membershipCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/memberships/job-label/jobs?labelID='.$testLabelID);

        $response->assertStatus(200);
        $response->assertJsonCount($membershipCount);
    }   
    
    /**
     * Tests MembershipJobLabel showJobs method with wrong path.
     *
     */
    public function testMemebershipJobLabelShowJobsInvalidPath()
    {
        $testLabelID = MembershipJobLabel::find(5)->id;  

        $response = $this->call('GET', '/api/memberships/job-labelInvalidPath/jobs?labelID='.$testLabelID);

        $response->assertStatus(404);
    }

    /**
     * Tests MembershipJobLabel showJobs method with wrong method call.
     *
     */
    public function testMemebershipJobLabelShowJobsInvalidMethod()
    {
        $response = $this->post('/api/memberships/job-label/jobs');

        $response->assertStatus(405);
    }

    /**
     * Tests MembershipJobLabel index method with invalid labelID.
     *
     */
    public function testMembershipJobLabelIndexInvalidLabelID()
    {
        $invalidLabelID = Label::all()->Count()+1;

        $response = $this->post('/api/memberships/job-labels/?budgetID='.$invalidLabelID);

        $response->assertStatus(405);
    }
        
    /**
     * Tests MembershipJobLabel index method with labelID not set.
     *
     */
    public function testMembershipJobLabelIndexLabelIDNotSet()
    {
        $response = $this->post('/api/memberships/job-labels/');

        $response->assertStatus(405);
    }


    /**
     * Tests MembershipJobLabel showJobs with query parameters.
     *
     */
    public function testMemebershipJobLabelShowJobsQueryParameter()
    {
        $testLabelID = MembershipJobLabel::find(5)->id;  
        
        $offset = 2;
        $limit = 6;

        $membershipCount = MembershipJobLabel::
        where('labelID', $testLabelID)
        ->get();
        $membershipCount = $membershipCount->slice($offset, $limit)->count(); 

        $response = $this->call('GET', '/api/memberships/job-label/jobs?labelID='.$testLabelID.'&offset='.$offset.'&limit='.$limit);
        $response->assertJsonCount($membershipCount);
        $response->assertStatus(200);

    }

    /**
     * Tests MembershipJobLabel showJobs with zero offset and index.
     *
     */
    public function testMemebershipJobLabelShowJobsIndexZeroLimit()
    {
        $testLabelID = MembershipJobLabel::find(5)->id;  

        $offset = -2;
        $limit = -4;
        $delete = DeleteEnum::ALL;
        
        $response = $this->call('GET', '/api/memberships/job-label/jobs?labelID='.$testLabelID.'&offset='.$offset.'&limit='.$limit);

        $response->assertJsonCount(0);
        $response->assertStatus(200);
    }    

    /**
     * Test MembershipJobLabel showJobs with invalid offset and limit.
     */
    public function testMemebershipJobLabelShowJobsInvalidOffsetLimit()
    {
        $testLabelID = MembershipJobLabel::find(5)->id;  
        $offset = "string";
        $limit = "false";

        $membershipCount = MembershipJobLabel::
        where('labelID', $testLabelID)
        ->get();
        $membershipCount = $membershipCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/memberships/job-label/jobs?labelID='.$testLabelID.'&offset='.$offset.'&limit='.$limit);

        $response->assertJsonCount($membershipCount);
        $response->assertStatus(200);
    }  
}
