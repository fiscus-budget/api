#!/bin/bash
# This script initializes a db (drops if already exists), a matching user and seeds it afterwards
# Note: Matching user refers to the .env configuration

if [ -f ".env" ];
then
    source .env
else
    echo "Please create a .env file before running this command"
    exit 1
fi

if [ "${DB_CONNECTION}" = "mysql" ];
then
    if [ "${DB_USERNAME}" = "root" ];
    then
        # Here we must fail, user root is not alowed in this script
        echo "Please use a different database user than root!"
        exit 3
    else
        # Create database -> before drop it
        MYSQL="/usr/bin/mysql --batch --skip-column-names --max_allowed_packet=100M -h${DB_HOST} -e"
        ${MYSQL} "DROP DATABASE IF EXISTS ${DB_DATABASE};"
        ${MYSQL} "CREATE DATABASE ${DB_DATABASE};"

        ${MYSQL} "DROP USER IF EXISTS '${DB_USERNAME}'@'localhost'"
        ${MYSQL} "CREATE USER '${DB_USERNAME}'@'localhost' IDENTIFIED BY '${DB_PASSWORD}';"
        ${MYSQL} "GRANT ALL PRIVILEGES ON * . * TO '${DB_USERNAME}'@'localhost';"

        # Seed data into db
        php artisan migrate:refresh --seed
    fi
else
    echo "Database not supported by this script"
    exit 2
fi