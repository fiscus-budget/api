<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateMonthlySavingRequest;
use App\Models\MonthlySaving;
use App\Helpers\ControllerHelper;
use DateTime;
use Illuminate\Http\Request;

class MonthlySavingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jobID= $request->query('jobID');
        $date= $request->query('date');
        $monthSaving =  MonthlySaving::where('jobID','=',$jobID)->get();

        foreach($monthSaving as $month){
            $month->jobID = $jobID;
            if (!empty($date)) {
                $month->monthDate = new DateTime($date);   
            } else{
                $month->monthDate = new DateTime();   
            }
        }

        return $monthSaving->append('ExpenseValue');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMonthlySavingRequest  $request
     * @param  int jobID
     * @param  string date
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMonthlySavingRequest $request, int $jobID, string $date)
    {

        $helper = new ControllerHelper; 
        $date = $helper->setQueryParameter(new DateTime($date), new DateTime());
        $assignedValue= $request->assignedValue;

        $existingSaving = MonthlySaving::where('jobID','=',$jobID)->whereMonth('monthYear', $date->format('m'))
        ->whereYear('monthYear', $date->format('Y'))
        ->get();

        if (count($existingSaving) > 0) {
            $existingSaving[0]->update([
                'assignedValue' => $assignedValue
            ]);
        }else{
            $existingSaving->add(MonthlySaving::create([
                'jobID' => $jobID,
                'monthYear' => $date->format('Y-m-d'),
                'assignedValue' => $assignedValue
            ]));
        }
        $existingSaving[0]->jobID = $jobID;

        return $existingSaving[0]->append('ExpenseValue');
    }
}
