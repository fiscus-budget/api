<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('budgetID')->nullable("false");
            $table->string('name');
            $table->integer('savingType');
            $table->boolean('delete');
            $table->double('valueGoal');
            $table->date('dateGoal')->nullable("true");
            $table->timestamps();            
            
            $table->foreign('budgetID')
            ->references('id')
            ->on('budgets')
            ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
