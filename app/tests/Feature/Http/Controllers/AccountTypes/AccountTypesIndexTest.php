<?php

namespace Tests\Feature\Http\Controllers\AccountTypes;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AccountTypesIndex extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Tests AccountType index.
     *
     */
    public function testAccountTypesIndex()
    {
        $response = $this->call('GET', '/api/accounttypes');
        $response->assertStatus(200);
    }   
    
    /**
     * Tests AccountType index method with wrong path.
     *
     */
    public function testAccountTypesIndexInvalidPath()
    {
        $response = $this->post('/api/accounttypesInvalidPath');

        $response->assertStatus(404);
    }

    /**
     * Tests AccountType index method with wrong method call.
     *
     */
    public function testAccountTypesIndexInvalidMethod()
    {
        $response = $this->post('/api/accounttypes');

        $response->assertStatus(405);
    }
}
