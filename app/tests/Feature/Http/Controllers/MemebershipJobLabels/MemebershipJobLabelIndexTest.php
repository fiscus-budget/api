<?php

namespace Tests\Feature\Http\Controllers\MemebershipJobLabels;

use App\Helpers\ControllerHelper;
use App\Enums\DeleteEnum;
use App\Models\MembershipJobLabel;
use App\Models\Budget;
use App\GlobalConstants\OffsetLimitConstant;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class MembershipJobLabelIndex extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Tests MembershipJobLabel index.
     *
     */
    public function testMembershipJobLabelIndex()
    {
        $testBudgetID = Budget::find(5)->id;

        $response = $this->call('GET', '/api/memberships/job-labels/?budgetID='.$testBudgetID);
        $response->assertStatus(200);
    }   
    
    /**
     * Tests MembershipJobLabel index method with wrong path.
     *
     */
    public function testMembershipJobLabelIndexInvalidPath()
    {
        $testBudgetID = Budget::find(5)->id;  

        $response = $this->post('/api/memberships/job-labelsInvalidPath/?budgetID='.$testBudgetID);

        $response->assertStatus(404);
    }

    /**
     * Tests MembershipJobLabel index method with invalid budgetID.
     *
     */
    public function testMembershipJobLabelIndexInvalidBudgetID()
    {
        $invalidBudgetID = Budget::all()->Count()+1;

        $response = $this->post('/api/memberships/job-labels/?budgetID='.$invalidBudgetID);

        $response->assertStatus(405);
    }
        
    /**
     * Tests MembershipJobLabel index method with budgetID not set.
     *
     */
    public function testMembershipJobLabelIndexBudgetIDNotSet()
    {
        $response = $this->post('/api/memberships/job-labels/');

        $response->assertStatus(405);
    }

    /**
     * Tests MembershipJobLabel index method with wrong method call.
     *
     */
    public function testMembershipJobLabelIndexInvalidMethod()
    {
        $response = $this->post('/api/memberships/job-labels');

        $response->assertStatus(405);
    }

    /**
     * Tests MembershipJobLabel index with query parameters.
     *
     */
    public function testMembershipJobLabelIndexQueryParameter()
    {
        $testBudgetID = Budget::find(5)->id;  
        
        $offset = 2;
        $limit = 6;
        $delete = DeleteEnum::ALL;

        $membershipCount = MembershipJobLabel::
        where('budgetID', $testBudgetID)
        ->get();
        $membershipCount = $membershipCount->slice($offset, $limit)->count(); 

        $response = $this->call('GET', '/api/memberships/job-labels?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit.'&delete='.$delete);
        $response->assertJsonCount($membershipCount);
        $response->assertStatus(200);

    }

    /**
     * Tests MembershipJobLabel index with zero offset and index.
     *
     */
    public function testMembershipJobLabelIndexZeroLimit()
    {
        $testBudgetID = Budget::find(5)->id;  

        $offset = -2;
        $limit = -4;
        $delete = DeleteEnum::ALL;
        
        $response = $this->call('GET', '/api/memberships/job-labels?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit.'&delete='.$delete);

        $response->assertJsonCount(0);
        $response->assertStatus(200);
    }    

    /**
     * Test MembershipJobLabel index with invalid offset and limit.
     */
    public function testMembershipJobLabelIndexInvalidOffsetLimit()
    {
        $testBudgetID = Budget::find(5)->id;  
        $offset = "string";
        $limit = "false";
        $delete = DeleteEnum::ALL;

        $membershipCount = MembershipJobLabel::
        where('budgetID', $testBudgetID)
        ->get();
        $membershipCount = $membershipCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/memberships/job-labels?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit.'&delete='.$delete);

        $response->assertJsonCount($membershipCount);
        $response->assertStatus(200);
    }  
}
