<?php

namespace Tests\Feature\Http\Controllers\Transactions;

use App\Helpers\ControllerHelper;
use App\Enums\DeleteEnum;
use App\Models\Transaction;
use App\Models\Budget;
use App\GlobalConstants\OffsetLimitConstant;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TransactionIndex extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Tests Transaction index.
     *
     */
    public function testTransactionIndex()
    {
        $testBudgetID = Budget::find(5)->id;

        $response = $this->call('GET', '/api/transactions/?budgetID='.$testBudgetID);
        $response->assertStatus(200);
    }   
    
    /**
     * Tests Transaction create method with wrong path.
     *
     */
    public function testTransactionIndexInvalidPath()
    {
        $testBudgetID = Budget::find(5)->id;  

        $response = $this->post('/api/transactionsInvalidPath/?budgetID='.$testBudgetID);

        $response->assertStatus(404);
    }

    /**
     * Tests Transaction create method with invalid budgetID.
     *
     */
    public function testTransactionIndexInvalidBudgetID()
    {
        $invalidBudgetID = Budget::all()->Count()+1;

        $response = $this->post('/api/transactions/?budgetID='.$invalidBudgetID);

        $response->assertStatus(405);
    }
        
    /**
     * Tests Transaction create method with budgetID not set.
     *
     */
    public function testTransactionIndexBudgetIDNotSet()
    {
        $response = $this->post('/api/transactions/');

        $response->assertStatus(405);
    }

    /**
     * Tests Transaction index method with wrong method call.
     *
     */
    public function testTransactionIndexInvalidMethod()
    {
        $response = $this->post('/api/transactions');

        $response->assertStatus(405);
    }

    /**
     * Tests Transaction index with query parameters.
     *
     */
    public function testTransactionIndexQueryParameter()
    {
        $testBudgetID = Budget::find(5)->id;  
        
        $offset = 2;
        $limit = 6;
        $delete = DeleteEnum::ALL;

        $transactionCount = Transaction::
        where('budgetID', $testBudgetID)
        ->get();
        $transactionCount = $transactionCount->slice($offset, $limit)->count(); 

        $response = $this->call('GET', '/api/transactions?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit.'&delete='.$delete);
        
        $response->assertJsonCount($transactionCount);
        $response->assertStatus(200);

    }

    /**
     * Tests Transaction index with zero offset and index.
     *
     */
    public function testTransactionIndexZeroLimit()
    {
        $testBudgetID = Budget::find(5)->id;  

        $offset = -2;
        $limit = -4;
        $delete = DeleteEnum::ALL;
        
        $response = $this->call('GET', '/api/transactions?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit.'&delete='.$delete);

        $response->assertJsonCount(0);
        $response->assertStatus(200);
    }    

    /**
     * Test Transaction index with invalid offset and limit.
     */
    public function testTransactionIndexInvalidOffsetLimit()
    {
        $testBudgetID = Budget::find(5)->id;  
        $offset = "string";
        $limit = "false";
        $delete = DeleteEnum::ALL;

        $transactionCount = Transaction::
        where('budgetID', $testBudgetID)
        ->get();
        $transactionCount = $transactionCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/transactions?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit.'&delete='.$delete);

        $response->assertJsonCount($transactionCount);
        $response->assertStatus(200);
    }  

    /**
     * Test Transaction index with delete = all.
    */
    public function testTransactionIndexDeleteEqualsAll()
    {        
        $helper = new ControllerHelper; 
        $testBudgetID = Budget::find(5)->id;  
        $delete = DeleteEnum::ALL;

        $transactionCount = Transaction::
        where('budgetID', $testBudgetID)
        ->get();
        $transactionCount = $transactionCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 

        $response = $this->call('GET', '/api/transactions?budgetID='.$testBudgetID.'&delete='.$delete);
  
        $response->assertJsonCount($transactionCount);
        $response->assertStatus(200);
    }  
    /**
     * Test Transaction index with delete = true.
     */
    public function testTransactionIndexDeleteEqualsTrue()
    {
        $testBudgetID = Budget::find(5)->id;  
        $helper = new ControllerHelper; 
        $delete = DeleteEnum::TRUE;

        $transactionCount = Transaction::
        where('budgetID', $testBudgetID)->
        where('delete', '=', $helper->reverseCastDelete($delete))->
        get();
        $transactionCount = $transactionCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/transactions?budgetID='.$testBudgetID.'&delete='.$delete);

        $response->assertJsonCount($transactionCount);
        $response->assertStatus(200);
    }  

    /**
     * Test Transaction index with delete = false;
    */
    public function testTransactionIndexDeleteEqualsFalse()
    {
        $testBudgetID = Budget::find(5)->id;  
        $helper = new ControllerHelper; 
        $delete = DeleteEnum::FALSE;

        $transactionCount = Transaction::
        where('budgetID', $testBudgetID)->
        where('delete', '=', $helper->reverseCastDelete($delete))->
        get();
        $transactionCount = $transactionCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/transactions?budgetID='.$testBudgetID.'&delete='.$delete);

        $response->assertJsonCount($transactionCount);
        $response->assertStatus(200);
    }  

    /**
     * Test Transaction index with invalid delete;
     */
    public function testTransactionIndexInvalidDelete()
    {
        $testBudgetID = Budget::find(5)->id;  
        $helper = new ControllerHelper; 
        $delete = "invalidDelete";
        $standardDelete= DeleteEnum::FALSE;

        $transactionCount = Transaction::
        where('budgetID', $testBudgetID)->
        where('delete', '=', $helper->reverseCastDelete($delete))->
        get();
        $transactionCount = $transactionCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/transactions?budgetID='.$testBudgetID.'&delete='.$delete);

        $response->assertJsonCount($transactionCount);
        $response->assertStatus(200);
    } 
}
