<?php

namespace Database\Seeders;

use App\Http\Controllers\MonthlySavingController;
use Illuminate\Database\Seeder;
use App\Models\MonthlySaving;
use DateTime;
use Illuminate\Support\Facades\DB;

class MonthlySavingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $newSavings = [[
            'jobID' => 21,
            'monthYear' => new DateTime('2021-12-01'),
            'assignedValue' => 350
        ],[
            'jobID' => 21,
            'monthYear' => new DateTime('2022-01-01'),
            'assignedValue' => 2000
        ],[
            'jobID' => 21,
            'monthYear' => new DateTime('2023-01-01'),
            'assignedValue' => 3000
        ]];

        MonthlySaving::factory()->times(500)->create();

        foreach($newSavings as $newSaving){
            MonthlySaving::Create($newSaving);
        }
    }
}
