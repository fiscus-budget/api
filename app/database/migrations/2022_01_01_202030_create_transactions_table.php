<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->unsignedInteger('budgetID')->nullable("false");
            $table->unsignedInteger('chargeAccountID')->nullable();
            $table->unsignedInteger('favorOfAccountID')->nullable();
            $table->unsignedInteger('chargeJobID')->nullable();
            $table->double('value');
            $table->boolean('delete');

            $table->timestamps();

            $table->foreign('budgetID')
            ->references('id')
            ->on('budgets')
            ->onDelete('set null'); 
            
            $table->foreign('chargeAccountID')
            ->references('id')
            ->on('accounts')
            ->onDelete('set null');

            $table->foreign('favorOfAccountID')
            ->references('id')
            ->on('accounts')
            ->onDelete('set null');

            $table->foreign('chargeJobID')
            ->references('id')
            ->on('jobs')
            ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
