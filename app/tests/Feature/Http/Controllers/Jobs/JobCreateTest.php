<?php

namespace Tests\Feature\Http\Controllers\Jobs;

use App\Models\Job;
use DateTime;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class JobCreate extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Test Job create.
     */
    public function testJobCreate()
    {
        $response = $this->post('/api/job',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            'savingType' => '2',
            'valueGoal' => '5000',
            'dateGoal' => '2023-10-26',
            'delete' => 'false'
        ]);
        $testJob = Job::latest('id')->first();

        $response->assertStatus(201);
        $response->assertSessionHasNoErrors();

        $this->assertEquals($response['id'], $testJob['id']);

        $this->assertDatabaseHas('jobs',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            'savingType' => '2',
            'valueGoal' => '5000',
            'dateGoal' => '2023-10-26',
        ]);
    }

    /**
     * Test Job create with invalid URL.
     */
    public function testJobCreateInvalidPath()
    {
        // dd('asd');
        $response = $this->post('/api/jobInvalidPath',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            'savingType' => '2',
            'valueGoal' => '5000',
            'dateGoal' => '2023-10-26',
            'delete' => 'false'
        ]);

        $response->assertStatus(404);
    }

    /**
     * Test Job with invalid method.
     */
    public function testJobCreateInvalidMethod()
    {
        $response = $this->get('/api/job',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            'savingType' => '2',
            'valueGoal' => '5000',
            'dateGoal' => '2023-10-26',
            'delete' => 'false'
        ]);

        $response->assertStatus(405);
    }

    /**
     * Test Job create with empty parameter.
    */
    public function testJobCreateValidationErrorEmpty()
    {
        $response = $this->post('/api/job',[
            "name" => '',
            "budgetID" => '',
            'savingType' => '',
            'valueGoal' => '',
            'dateGoal' => ''
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'name' => 'The name field is required.',
            'budgetID' => 'The budget i d field is required.',
            'savingType' => 'The saving type field is required.',
            'valueGoal' => 'The value goal field is required.',
            // 'valueGoal' => 'The date goal is not a valid date.',
            // 'valueGoal' => 'The date goal does not match the format Y-m-d.',
        ]);
    }
        
    /**
     * Test Job create with invalid parameter type.
    */
    public function testJobCreateValidationInvalidType()
    {
        $response = $this->post('/api/job',[
            "name" => true,
            "budgetID" => "String",
            'savingType' => 'String',
            'valueGoal' => 'String',
            'dateGoal' => true
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'name' => 'The name must be a string.',
            "valueGoal" => "The value goal must be a number.",
            "valueGoal" => "The value goal format is invalid.",
            "dateGoal"=> "The date goal is not a valid date.",
            "dateGoal"=> "The date goal does not match the format Y-m-d.",
            "budgetID"=> "The budget i d must be an integer.",
            "savingType"=> "The saving type must be an integer.",
            "savingType"=> "The saving type must not be greater than 2."
        ]);
    }
        
    /**
     * Test Job create with invalid value length.
    */
    public function testJobCreateValidationInvalidValueLength()
    {
        $response = $this->post('/api/job',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            "valueGoal" => "-9999999999999999999.99",
            "savingType" => "4",
            "dateGoal" => "2023-10-26"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'valueGoal' => 'The value goal must be between -999999999999999999.99 and 999999999999999999.99.',
            'savingType' => 'The saving type must not be greater than 2.'
        ]);
    }

    /**
     * Test Job create with savingType OnDate.
    */
    public function testJobCreateValidationSavingTypeOnDate()
    {
        $response = $this->post('/api/job',[
            "name" => "testJobCreate",
            "budgetID" => "2",
            "valueGoal" => "324",
            "savingType" => "0",
            "dateGoal" => ""
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'dateGoal' => 'The date goal is required.'
        ]);
    }
}
