<?php

namespace Tests\Feature\Http\Controllers\Budgets;
use App\Helpers\ControllerHelper;
use App\Enums\DeleteEnum;
use App\Models\Budget;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BudgetUpdate extends TestCase
{
    use DatabaseTransactions;
    /**
     * Test Budget update.
     */
    public function testBudgetUpdate()
    {
        $this->post('/api/budget',[
            "name" => "testBudgetUpdate"
        ]);

        $testBudget = Budget::latest('id')->first();

        $response = $this->put('/api/budget/'.$testBudget['id'],[
            "name" => "testBudgetUpdate"
        ]);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();

        $this->assertEquals($response['id'], $testBudget['id']);

        $this->assertDatabaseHas('budgets',[
            'name' => 'testBudgetUpdate'
        ]);
    }

    /**
     * Test Budget update invalid URL
    */
    public function testBudgetUpdateInvalidURL()
    {
        $this->post('/api/budget',[
            "name" => "testBudgetUpdate"
        ]);

        $testBudget = Budget::latest('id')->first();

        $response = $this->put('/api/budgetInvalidURL/'.$testBudget['id'],[
            "name" => "testBudgetUpdate"
        ]);

        $response->assertStatus(404);
    }

    /**
     * Test Budget update invalid method.
     */
    public function testBudgetUpdateInvalidMethod()
    {
        $this->post('/api/budget',[
            "name" => "testBudgetUpdate"
        ]);

        $testBudget = Budget::latest('id')->first();

        $response = $this->post('/api/budget/'.$testBudget['id'],[
            "name" => "testBudgetUpdate"
        ]);

        $response->assertStatus(405);
    }

    /**
     * Test Budget update validation error.
     */
    public function testBudgetUpdateValidationError()
    {
        $this->post('/api/budget',[
            "name" => "testBudgetUpdate"
        ]);
        $testBudget = Budget::latest('id')->first();

        $response = $this->put('/api/budget/'.$testBudget['id'],[
            "name" => ""
        ]);
        $response->assertStatus(302);
        $response->assertSessionHasErrors();
        $response->assertSessionHasErrors([
            'name' => 'The name field is required.'
        ]);

        $response = $this->put('/api/budget/'.$testBudget['id'],[
            "name" => "This text is way to long for a simple name of a budget."
        ]);
        $response->assertStatus(302);
        $response->assertSessionHasErrors();
        $response->assertSessionHasErrors([
            'name' => 'The name must not be greater than 30 characters.'
        ]);
    }
}
