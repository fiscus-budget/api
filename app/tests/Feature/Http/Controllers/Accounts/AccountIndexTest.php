<?php

namespace Tests\Feature\Http\Controllers\Accounts;

use App\Helpers\ControllerHelper;
use App\Enums\DeleteEnum;
use App\Models\Account;
use App\Models\Budget;
use App\GlobalConstants\OffsetLimitConstant;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AccountIndex extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Tests Account index.
     *
     */
    public function testAccountIndex()
    {
        $testBudgetID = Budget::find(5)->id;

        $response = $this->call('GET', '/api/accounts/?budgetID='.$testBudgetID);
        $response->assertStatus(200);
    }   
    
    /**
     * Tests Account index method with wrong path.
     *
     */
    public function testAccountIndexInvalidPath()
    {
        $testBudgetID = Budget::find(5)->id;  

        $response = $this->post('/api/accountsInvalidPath/?budgetID='.$testBudgetID);

        $response->assertStatus(404);
    }

    /**
     * Tests Account index method with invalid budgetID.
     *
     */
    public function testAccountIndexInvalidBudgetID()
    {
        $invalidBudgetID = Budget::all()->Count()+1;

        $response = $this->post('/api/accounts/?budgetID='.$invalidBudgetID);

        $response->assertStatus(405);
    }
        
    /**
     * Tests Account index method with budgetID not set.
     *
     */
    public function testAccountIndexBudgetIDNotSet()
    {
        $response = $this->post('/api/accounts/');

        $response->assertStatus(405);
    }

    /**
     * Tests Account index method with wrong method call.
     *
     */
    public function testAccountIndexInvalidMethod()
    {
        $response = $this->post('/api/accounts');

        $response->assertStatus(405);
    }

    /**
     * Tests Account index with query parameters.
     *
     */
    public function testAccountIndexQueryParameter()
    {
        $testBudgetID = Budget::find(5)->id;  
        
        $offset = 2;
        $limit = 6;
        $delete = DeleteEnum::ALL;

        $accountCount = Account::
        where('budgetID', $testBudgetID)
        ->get();
        $accountCount = $accountCount->slice($offset, $limit)->count(); 

        $response = $this->call('GET', '/api/accounts?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit.'&delete='.$delete);
        $response->assertJsonCount($accountCount);
        $response->assertStatus(200);

    }

    /**
     * Tests Account index with zero offset and index.
     *
     */
    public function testAccountIndexZeroLimit()
    {
        $testBudgetID = Budget::find(5)->id;  

        $offset = -2;
        $limit = -4;
        $delete = DeleteEnum::ALL;
        
        $response = $this->call('GET', '/api/accounts?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit.'&delete='.$delete);

        $response->assertJsonCount(0);
        $response->assertStatus(200);
    }    

    /**
     * Test Account index with invalid offset and limit.
     */
    public function testAccountIndexInvalidOffsetLimit()
    {
        $testBudgetID = Budget::find(5)->id;  
        $offset = "string";
        $limit = "false";
        $delete = DeleteEnum::ALL;

        $accountCount = Account::
        where('budgetID', $testBudgetID)
        ->get();
        $accountCount = $accountCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/accounts?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit.'&delete='.$delete);

        $response->assertJsonCount($accountCount);
        $response->assertStatus(200);
    }  

    /**
     * Test Account index with delete = all.
    */
    public function testAccountIndexDeleteEqualsAll()
    {        
        $helper = new ControllerHelper; 
        $testBudgetID = Budget::find(5)->id;  
        $delete = DeleteEnum::ALL;

        $accountCount = Account::
        where('budgetID', $testBudgetID)
        ->get();
        $accountCount = $accountCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 

        $response = $this->call('GET', '/api/accounts?budgetID='.$testBudgetID.'&delete='.$delete);
  
        $response->assertJsonCount($accountCount);
        $response->assertStatus(200);
    }  
    
    /**
     * Test Account index with delete = true.
     */
    public function testAccountIndexDeleteEqualsTrue()
    {
        $testBudgetID = Budget::find(5)->id;  
        $helper = new ControllerHelper; 
        $delete = DeleteEnum::TRUE;

        $accountCount = Account::
        where('budgetID', $testBudgetID)->
        where('delete', '=', $helper->reverseCastDelete($delete))->
        get();
        
        $accountCount = $accountCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/accounts?budgetID='.$testBudgetID.'&delete='.$delete);

        $response->assertJsonCount($accountCount);
        $response->assertStatus(200);
    }  

    /**
     * Test Account index with delete = false;
    */
    public function testAccountIndexDeleteEqualsFalse()
    {
        $testBudgetID = Budget::find(5)->id;  
        $helper = new ControllerHelper; 
        $delete = DeleteEnum::FALSE;

        $accountCount = Account::
        where('budgetID', $testBudgetID)->
        where('delete', '=', $helper->reverseCastDelete($delete))->
        get();
        
        $accountCount = $accountCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 

        $response = $this->call('GET', '/api/accounts?budgetID='.$testBudgetID.'&delete='.$delete);

        $response->assertJsonCount($accountCount);
        $response->assertStatus(200);
    }  

    /**
     * Test Account index with invalid delete;
     */
    public function testAccountIndexInvalidDelete()
    {
        $testBudgetID = Budget::find(5)->id;  
        $helper = new ControllerHelper; 
        $delete = "invalidDelete";
        $standardDelete= DeleteEnum::FALSE;

        $accountCount = Account::
        where('budgetID', $testBudgetID)->
        where('delete', '=', $helper->reverseCastDelete($standardDelete))->
        get();
        
        $accountCount = $accountCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 

        $response = $this->call('GET', '/api/accounts?budgetID='.$testBudgetID.'&delete='.$delete);

        $response->assertJsonCount($accountCount);
        $response->assertStatus(200);
    }  
}
