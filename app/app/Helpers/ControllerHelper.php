<?php

namespace App\Helpers;

class ControllerHelper 
{
    /**
     * Sets query parameter if not empty or given default value.
     *
     * @return var
     */
    public static function setQueryParameter($queryParameter, $defaultValue){
        if (!empty($queryParameter)) {
            return $queryParameter;
        }
        return $defaultValue;
    }

    /**
     * Sets integer or, if no integer given, default integer.
     *
     * @return int
     */
    public static function checkIntVal($checkInt, $defaultIntValue){
        if (intval($checkInt)) {
            return $checkInt;
        }
        return $defaultIntValue;
    }

    /**
     * Reverse delete boolean cast to integer.
     *
     * @return integer
     */
    public static function reverseCastDelete($boolValue){
        if ($boolValue === "true") {
            return 1;
        }
        return 0;
    }

}