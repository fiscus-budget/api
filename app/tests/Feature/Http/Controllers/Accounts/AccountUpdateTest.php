<?php

namespace Tests\Feature\Http\Controllers\Accounts;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Account;
use Tests\TestCase;

class AccountUpdate extends TestCase
{
    use DatabaseTransactions;
    /**
     * Test Account update.
     */
    public function testAccountUpdate()
    {
        $this->post('/api/account',[
            "name" => "testAccountUpdate",
            "budgetID" => "2",
            "value" => "546",
            "typeID" => "3"
        ]);

        $testAccount = Account::latest('id')->first();

        $response = $this->put('/api/account/'.$testAccount['id'],[
            "name" => "newTestAccountUpdate",
            "value" => "544",
            "typeID" => "1"
        ]);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();

        $this->assertEquals($response['id'], $testAccount['id']);

        $this->assertDatabaseHas('accounts',[
            "name" => "newTestAccountUpdate",
            "budgetID" => "2",
            "value" => "544",
            "typeID" => "1"
        ]);
    }

    /**
     * Test Account update invalid URL
    */
    public function testAccountUpdateInvalidURL()
    {
        $this->post('/api/account',[
            "name" => "testAccountUpdate",
            "budgetID" => "2",
            "value" => "546",
            "typeID" => "3"
        ]);

        $testAccount = Account::latest('id')->first();

        $response = $this->put('/api/accountInvalidURL/'.$testAccount['id'],[
            "name" => "newTestAccountUpdate",
            "value" => "544",
            "typeID" => "1"
        ]);

        $response->assertStatus(404);
    }

    /**
     * Test Account update invalid method.
     */
    public function testAccountUpdateInvalidMethod()
    {
        $this->post('/api/account',[
            "name" => "testAccountUpdate",
            "budgetID" => "2",
            "value" => "546",
            "typeID" => "3"
        ]);

        $testAccount = Account::latest('id')->first();

        $response = $this->post('/api/account/'.$testAccount['id'],[
            "name" => "newTestAccountUpdate",
            "value" => "544",
            "typeID" => "1"
        ]);

        $response->assertStatus(405);
    }

    /**
     * Test Account update with empty parameter.
    */
    public function testAccountUpdateValidationErrorEmpty()
    {
        $this->post('/api/account',[
            "name" => "testAccountUpdate",
            "budgetID" => "2",
            "value" => "546",
            "typeID" => "3"
        ]);
        $testAccount = Account::latest('id')->first();

        $response = $this->put('/api/account/'.$testAccount['id'],[
            "name" => "",
            "value" => "",
            "typeID" => ""
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'name' => 'The name field is required.',
            'value' => 'The value field is required.',
            'typeID' => 'The type i d field is required.',
        ]);
    }
        
    /**
     * Test Account update with invalid parameter type.
    */
    public function testAccountUpdateValidationInvalidType()
    {
        $this->post('/api/account',[
            "name" => "testAccountUpdate",
            "budgetID" => "2",
            "value" => "546",
            "typeID" => "3"
        ]);
        $testAccount = Account::latest('id')->first();

        $response = $this->put('/api/account/'.$testAccount['id'],[
            "name" => true,
            "budgetID" => "4",
            "typeID" => "String",
            "value" => "String"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'name' => 'The name must be a string.',
            'budgetID' => 'The budget i d field is prohibited.',
            'value' => 'The value must be a number.',
            'value' => 'The value format is invalid.',
            'typeID' => 'The type i d must be an integer.',
        ]);
    }
        
    /**
     * Test Account update with invalid value length.
    */
    public function testAccountUpdateValidationInvalidValueLength()
    {
        $this->post('/api/account',[
            "name" => "testAccountUpdate",
            "budgetID" => "2",
            "value" => "546",
            "typeID" => "3"
        ]);
        $testAccount = Account::latest('id')->first();

        $response = $this->put('/api/account/'.$testAccount['id'],[
            "name" => "newTestAccountUpdate",
            "value" => "-9999999999999999999.99",
            "typeID" => "3"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'value' => 'The value must be between -999999999999999999.99 and 999999999999999999.99.'
        ]);
    }
}
