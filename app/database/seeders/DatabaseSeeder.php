<?php

namespace Database\Seeders;

use App\Models\Transaction;
use Illuminate\Database\Seeder;
use Database\Seeders\BudgetSeeder;
use Database\Seeders\AccountSeeder;
use Database\Seeders\AccountTypeSeeder;
use Database\Seeders\TransactionSeeder;
use Database\Seeders\JobSeeder;
use Database\Seeders\LabelSeeder;
use Database\Seeders\MembershipJobLabelSeeder;
use Database\Seeders\MonthlySavingSeeder;



class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BudgetSeeder::class);
        $this->call(AccountTypeSeeder::class);
        $this->call(AccountSeeder::class);
        $this->call(JobSeeder::class);
        $this->call(TransactionSeeder::class);
        $this->call(LabelSeeder::class);
        $this->call(MembershipJobLabelSeeder::class);
        $this->call(MonthlySavingSeeder::class);
    }
}
