<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MembershipJobLabelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'budgetID' => $this->faker->numberBetween($min = 1, $max = 10),
            'jobID' => $this->faker->numberBetween($min = 1, $max = 20),
            'labelID' => $this->faker->numberBetween($min = 1, $max = 20),
        ];
    }
}
