<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTransactionRequest;
use App\Http\Requests\UpdateTransactionRequest;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Helpers\ControllerHelper;
use App\GlobalConstants\OffsetLimitConstant;
use App\Enums\DeleteEnum;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $helper = new ControllerHelper; 
        $offset = $helper->setQueryParameter($request->query("offset"), OffsetLimitConstant::OFFSET);
        $offset = $helper->checkIntVal($offset, OffsetLimitConstant::OFFSET);
        $limit = $helper->setQueryParameter($request->query("limit"), OffsetLimitConstant::LIMIT);
        $limit = $helper->checkIntVal($limit, OffsetLimitConstant::LIMIT);
        $delete = $helper->setQueryParameter($request->query("delete"), DeleteEnum::FALSE);

        $budgetID = $request->query('budgetID');

        if ($delete == DeleteEnum::ALL) {
            $indexTransactions =  Transaction::
            with('chargeAccount')->
            with('favorOfAccount')->
            with('chargeJob')->
            where('budgetID', $budgetID)->
            orderBy('date', 'ASC')->get();
        } else{
            $indexTransactions = Transaction::
            with('chargeAccount')->
            with('favorOfAccount')->
            with('chargeJob')->
            where('budgetID', $budgetID)->
            where('delete', '=', $helper->reverseCastDelete($delete))->
            orderBy('date', 'ASC')->get();
        }

        $slicedTransactions = $indexTransactions->slice($offset, $limit);

        $displayTransactions= []; 

        foreach($slicedTransactions as $transaction){
            array_push($displayTransactions, $transaction);
        }


        return $displayTransactions;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTransactionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTransactionRequest $request)
    {
            $newTransaction = Transaction::create([
            'budgetID' => $request->input('budgetID'),
            'date' => $request->input('date'),
            'delete' => DeleteEnum::FALSE,
            //API change to chargeAccountID
            'chargeAccountID' => $request->input('chargeAccountID'),
            'favorOfAccountID' => $request->input('favorOfAccountID'),
            'chargeJobID' => $request->input('chargeJobID'),
            'value' => $request->input('value'),
        ]);

        $newTransaction->transfer(
            $request->input('chargeAccountID'),
            $request->input('favorOfAccountID'), 
            $request->input('chargeJobID'),
            $request->input('value'));

        return $newTransaction;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $transactionID
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $transactionID)
    {
        $helper = new ControllerHelper; 
        $delete = $helper->setQueryParameter($request->query("delete"), DeleteEnum::FALSE);

        $showTransaction = Transaction::
        with('chargeAccount')->
        with('favorOfAccount')->
        with('chargeJob')->
        where('delete', '=', $delete)->
        find($transactionID);

        return $showTransaction;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $transactionID
     * @param  \App\Http\Requests\UpdateTransactionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTransactionRequest $request, $transactionID)
    {
        
        $updateTransaction = Transaction::find($transactionID);
        if (
            $updateTransaction->date != $request->input('date') && 
            $updateTransaction->chargeAccountID == $request->input('chargeAccountID') && 
            $updateTransaction->favorOfAccountID == $request->input('favorOfAccountID') && 
            $updateTransaction->chargeJobID == $request->input('chargeJobID') && 
            $updateTransaction->value == $request->input('value') 
        
        ) {
            $updateTransaction->update([
                'date' => $request->input('date'),
            ]);
        }
        else {
            $updateTransaction->revertTransfer($transactionID);

            $updateTransaction->update([
                'date' => $request->input('date'),
                'chargeAccountID' => $request->input('chargeAccountID'),
                'favorOfAccountID' => $request->input('favorOfAccountID'),
                'chargeJobID' => $request->input('chargeJobID'),
                'value' => $request->input('value'),
            ]);
            $updateTransaction->transfer(
                $updateTransaction->chargeAccountID,
                $updateTransaction->favorOfAccountID, 
                $updateTransaction->chargeJobID,
                $updateTransaction->value
            );
        }

        return $updateTransaction;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $transactionID
     * @return \Illuminate\Http\Response
     */
    public function destroy($transactionID)
    {        
            $deleteTransaction = Transaction::find($transactionID);

        if ($deleteTransaction->delete == DeleteEnum::FALSE) {
            $this->flagDelete($transactionID);
        }

        Transaction::destroy($transactionID);
    }

     /**
     * Flags the specified resource as delete.
     *
     * @param  int $transactionID
     * @return \Illuminate\Http\Response
     */
    public function flagDelete($transactionID)
    {
        $deleteTransaction = Transaction::find($transactionID);

        $deleteTransaction->update([
            'delete' => DeleteEnum::TRUE,
        ]);
        $deleteTransaction->revertTransfer($transactionID);

        return $deleteTransaction;
    }
}
