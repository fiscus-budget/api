<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Transaction;
use DateTime;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transactions = [
            [
                'budgetID' => 3,
                'date' => new DateTime('2021-12-01'),
                'chargeAccountID' => 3,
                'favorOfAccountID' => 2,
                'chargeJobID' => 21,
                'value' => -400,
                'delete' => false
                ],
                [
                    'budgetID' => 3,
                    'date' => new DateTime('2021-12-01'),
                    'chargeAccountID' => 3,
                    'favorOfAccountID' => 2,
                    'chargeJobID' => 21,
                    'value' => -10000,
                    'delete' => true
                ],
                [
                    'budgetID' => 3,
                    'date' => new DateTime('2022-01-01'),
                    'chargeAccountID' => 3,
                    'favorOfAccountID' => 2,
                    'chargeJobID' => 21,
                    'value' => -100,
                    'delete' => false
                    ]
                    ,
                [
                    'budgetID' => 3,
                    'date' => new DateTime('2023-02-01'),
                    'chargeAccountID' => 3,
                    'favorOfAccountID' => 2,
                    'chargeJobID' => 21,
                    'value' => -650,
                    'delete' => false
                    ]
                ];

        Transaction::factory()->times(300)->create();

        foreach($transactions as $transaction){
            Transaction::Create($transaction);
        };
    }   
}
