<?php

namespace Tests\Feature\Http\Controllers\Accounts;

use App\Models\Account;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AccountCreate extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Test Account create.
     */
    public function testAccountCreate()
    {
        $response = $this->post('/api/account',[
            "name" => "testAccountCreate",
            "budgetID" => "2",
            "value" => "546",
            "typeID" => "3"
        ]);
        $testAccount = Account::latest('id')->first();

        $response->assertStatus(201);
        $response->assertSessionHasNoErrors();

        $this->assertEquals($response['id'], $testAccount['id']);

        $this->assertDatabaseHas('accounts',[
            "name" => "testAccountCreate",
            "budgetID" => "2",
            "value" => "546",
            "typeID" => "3"
        ]);
    }

    /**
     * Test Account create with invalid URL.
     */
    public function testAccountCreateInvalidPath()
    {
        $response = $this->post('/api/accountInvalidPath',[
            "name" => "testAccountCreate",
            "budgetID" => "2",
            "value" => "546",
            "typeID" => "3"
        ]);

        $response->assertStatus(404);
    }

    /**
     * Test Account with invalid method.
     */
    public function testAccountCreateInvalidMethod()
    {
        $response = $this->get('/api/account',[
            "name" => "testAccountCreate",
            "budgetID" => "2",
            "value" => "546",
            "typeID" => "3"
        ]);

        $response->assertStatus(405);
    }

    /**
     * Test Account create with empty parameter.
    */
    public function testAccountCreateValidationErrorEmpty()
    {
        $response = $this->post('/api/account',[
            "name" => "",
            "budgetID" => "",
            "typeID" => "",
            "value" => ""
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'name' => 'The name field is required.',
            'budgetID' => 'The budget i d field is required.',
            'typeID' => 'The type i d field is required.',
        ]);
    }
        
    /**
     * Test Account create with invalid parameter type.
    */
    public function testAccountCreateValidationInvalidType()
    {
        $response = $this->post('/api/account',[
            "name" => true,
            "budgetID" => "String",
            "typeID" => "String",
            "value" => "String"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'name' => 'The name must be a string.',
            'budgetID' => 'The budget i d must be an integer.',
            'value' => 'The value must be a number.',
            'value' => 'The value format is invalid.',
            'typeID' => 'The type i d must be an integer.',
        ]);
    }
        
    /**
     * Test Account create with invalid value length.
    */
    public function testAccountCreateValidationInvalidValueLength()
    {
        $response = $this->post('/api/account',[
            "name" => "testAccountCreate",
            "budgetID" => "2",
            "value" => "-9999999999999999999.99",
            "typeID" => "3"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'value' => 'The value must be between -999999999999999999.99 and 999999999999999999.99.'
        ]);
    }
}
