<?php

namespace App\Http\Controllers;

use App\Models\AccountType;
use Illuminate\Http\Request;

class AccountTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AccountType::all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $accounttypeID
     * @return \Illuminate\Http\Response
     */
    public function show(int $accounttypeID)
    {
        return AccountType::find($accounttypeID);
    }
}
