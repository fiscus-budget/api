<?php

namespace Tests\Feature\Http\Controllers\MemebershipJobLabels;

use App\Models\MembershipJobLabel;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class MembershipJobLabelCreate extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Test MembershipJobLabel create.
     */
    public function testMembershipJobLabelCreate()
    {
        $response = $this->post('/api/memberships/job-label',[
            "budgetID" => "2",
            "jobID" => "17",
            "labelID" => "3"
        ]);
        $testMembership = MembershipJobLabel::latest('id')->first();

        $response->assertStatus(201);
        $response->assertSessionHasNoErrors();

        $this->assertEquals($response['id'], $testMembership['id']);

        $this->assertDatabaseHas('membership_job_labels',[
            "budgetID" => "2",
            "jobID" => "17",
            "labelID" => "3"
        ]);
    }

    /**
     * Test MembershipJobLabel create with invalid URL.
     */
    public function testMembershipJobLabelCreateInvalidPath()
    {
        $response = $this->post('/api/memberships/job-labelInvalidPath',[
            "budgetID" => "2",
            "jobID" => "17",
            "labelID" => "3"
        ]);

        $response->assertStatus(404);
    }

    /**
     * Test MembershipJobLabel with invalid method.
     */
    public function testMembershipJobLabelCreateInvalidMethod()
    {
        $response = $this->get('/api/memberships/job-label',[
            "budgetID" => "2",
            "jobID" => "17",
            "labelID" => "3"
        ]);

        $response->assertStatus(405);
    }

    /**
     * Test MembershipJobLabel create with empty parameter.
    */
    public function testMembershipJobLabelCreateValidationErrorEmpty()
    {
        $response = $this->post('/api/memberships/job-label',[
            "budgetID" => "",
            "jobID" => "",
            "labelID" => ""
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'budgetID' => 'The budget i d field is required.',
            'jobID' => 'The job i d field is required.',
            'labelID' => 'The label i d field is required.',
        ]);
    }
        
    /**
     * Test MembershipJobLabel create with invalid parameter type.
    */
    public function testMembershipJobLabelCreateValidationInvalidType()
    {
        $response = $this->post('/api/memberships/job-label',[
            "budgetID" => "String",
            "jobID" => "String",
            "labelID" => "String"
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors();

        $response->assertSessionHasErrors([
            'budgetID' => 'The budget i d must be an integer.',
            'jobID' => 'The job i d must be an integer.',
            'labelID' => 'The label i d must be an integer.',
        ]);
    }
}
