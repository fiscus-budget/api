<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBudgetRequest;
use App\Http\Requests\UpdateBudgetRequest;
use App\Models\Budget;
use Illuminate\Http\Request;
use App\Enums\DeleteEnum;
use App\GlobalConstants\OffsetLimitConstant;
use App\Helpers\ControllerHelper;

class BudgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $helper = new ControllerHelper();
        $offset = $helper->setQueryParameter($request->query("offset"), OffsetLimitConstant::OFFSET);
        $offset = $helper->checkIntVal($offset, OffsetLimitConstant::OFFSET);
        $limit = $helper->setQueryParameter($request->query("limit"), OffsetLimitConstant::LIMIT);
        $limit = $helper->checkIntVal($limit, OffsetLimitConstant::LIMIT);
        $delete = $helper->setQueryParameter($request->query("delete"), DeleteEnum::FALSE);
        
        if ($delete == DeleteEnum::ALL) {
            $indexBudgets =  Budget::
            orderBy('id', 'ASC')->get();
        } else{
            $indexBudgets = Budget::
            where('delete', '=', $helper->reverseCastDelete($delete))->
            orderBy('id', 'ASC')->get();
        }

        $indexBudgets = $indexBudgets->slice($offset, $limit);
        $displayBudgets = []; 

        foreach($indexBudgets as $budget){
            array_push($displayBudgets, $budget);
        }

        return $displayBudgets;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBudgetRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBudgetRequest $request)
    {
        $newBudget = Budget::create([
            'delete' => DeleteEnum::FALSE,
            'name' => $request->input('name')
        ]);
        return $newBudget;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $budgetID
     * @return \Illuminate\Http\Response
     */
    public function show($budgetID)
    {
        return Budget::find($budgetID);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBudgetRequest  $request
     * @param  int $budgetID
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBudgetRequest $request, $budgetID)
    {
        $updateBudget = Budget::find($budgetID);
        $updateBudget->update($request->all());
        return $updateBudget;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($budgetID)
    {
        return Budget::destroy($budgetID);
    }

     /**
     * Flags the specified resource as delete.
     *
     * @param  int $budgetID
     * @return \Illuminate\Http\Response
     */
    public function flagDelete($budgetID)
    {
        $deleteBudget = Budget::find($budgetID);
        $deleteBudget->update([
            'delete' => DeleteEnum::TRUE
        ]);
        return $deleteBudget;
    }

}
