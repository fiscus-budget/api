<?php

namespace App\Http\Requests;

use App\Rules\DateGoalRequired;
use Illuminate\Foundation\Http\FormRequest;

class StoreJobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required|string|min:1|max:30', 
            'valueGoal' => 'required|numeric|between:-999999999999999999.99,999999999999999999.99|regex:/^[-]?[0-9]*.[0-9]?[0-9]?$/',
            'dateGoal' => [new DateGoalRequired($this->savingType),'date', 'date_format:Y-m-d'],
            'budgetID' => 'required|integer|exists:budgets,id',
            'savingType' => 'required|integer|min:0|max:2',
        ];
    }
}
