<?php

namespace Tests\Feature\Http\Controllers\Jobs;

use App\Helpers\ControllerHelper;
use App\Enums\DeleteEnum;
use App\Models\Job;
use App\Models\Budget;
use App\GlobalConstants\OffsetLimitConstant;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class JobIndex extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Tests Job index.
     *
     */
    public function testJobIndex()
    {
        $testBudgetID = Budget::find(5)->id;

        $response = $this->call('GET', '/api/jobs/?budgetID='.$testBudgetID);
        $response->assertStatus(200);
    }   
    
    /**
     * Tests Job index method with wrong path.
     *
     */
    public function testJobIndexInvalidPath()
    {
        $testBudgetID = Budget::find(5)->id;  

        $response = $this->post('/api/jobsInvalidPath/?budgetID='.$testBudgetID);

        $response->assertStatus(404);
    }

    /**
     * Tests Job index method with invalid budgetID.
     *
     */
    public function testJobIndexInvalidBudgetID()
    {
        $invalidBudgetID = Budget::all()->Count()+1;

        $response = $this->post('/api/jobs/?budgetID='.$invalidBudgetID);

        $response->assertStatus(405);
    }
        
    /**
     * Tests Job index method with budgetID not set.
     *
     */
    public function testJobIndexBudgetIDNotSet()
    {
        $response = $this->post('/api/jobs/');

        $response->assertStatus(405);
    }

    /**
     * Tests Job index method with wrong method call.
     *
     */
    public function testJobIndexInvalidMethod()
    {
        $response = $this->post('/api/jobs');

        $response->assertStatus(405);
    }

    /**
     * Tests Job index with query parameters.
     *
     */
    public function testJobIndexQueryParameter()
    {
        $testBudgetID = Budget::find(5)->id;  
        
        $offset = 2;
        $limit = 6;
        $delete = DeleteEnum::ALL;

        $jobCount = Job::
        where('budgetID', $testBudgetID)
        ->get();
        $jobCount = $jobCount->slice($offset, $limit)->count(); 

        $response = $this->call('GET', '/api/jobs?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit.'&delete='.$delete);
        
        $response->assertJsonCount($jobCount);
        $response->assertStatus(200);

    }

    /**
     * Tests Job index with zero offset and index.
     *
     */
    public function testJobIndexZeroLimit()
    {
        $testBudgetID = Budget::find(5)->id;  

        $offset = -2;
        $limit = -4;
        $delete = DeleteEnum::ALL;
        
        $response = $this->call('GET', '/api/jobs?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit.'&delete='.$delete);

        $response->assertJsonCount(0);
        $response->assertStatus(200);
    }    

    /**
     * Test Job index with invalid offset and limit.
     */
    public function testJobIndexInvalidOffsetLimit()
    {
        $testBudgetID = Budget::find(5)->id;  
        $offset = "string";
        $limit = "false";
        $delete = DeleteEnum::ALL;

        $jobCount = Job::
        where('budgetID', $testBudgetID)
        ->get();
        $jobCount = $jobCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/jobs?budgetID='.$testBudgetID.'&offset='.$offset.'&limit='.$limit.'&delete='.$delete);

        $response->assertJsonCount($jobCount);
        $response->assertStatus(200);
    }  

    /**
     * Test Job index with delete = all.
    */
    public function testJobIndexDeleteEqualsAll()
    {        
        $helper = new ControllerHelper; 
        $testBudgetID = Budget::find(5)->id;  
        $delete = DeleteEnum::ALL;

        $jobCount = Job::
        where('budgetID', $testBudgetID)
        ->get();
        $jobCount = $jobCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 

        $response = $this->call('GET', '/api/jobs?budgetID='.$testBudgetID.'&delete='.$delete);
  
        $response->assertJsonCount($jobCount);
        $response->assertStatus(200);
    }  
    /**
     * Test Job index with delete = true.
     */
    public function testJobIndexDeleteEqualsTrue()
    {
        $testBudgetID = Budget::find(5)->id;  
        $helper = new ControllerHelper; 
        $delete = DeleteEnum::TRUE;

        $jobCount = Job::
        where('budgetID', $testBudgetID)->
        where('delete', '=', $helper->reverseCastDelete($delete))->
        get();
        $jobCount = $jobCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/jobs?budgetID='.$testBudgetID.'&delete='.$delete);

        $response->assertJsonCount($jobCount);
        $response->assertStatus(200);
    }  

    /**
     * Test Job index with delete = false;
    */
    public function testJobIndexDeleteEqualsFalse()
    {
        $testBudgetID = Budget::find(5)->id;  
        $helper = new ControllerHelper; 
        $delete = DeleteEnum::FALSE;

        $jobCount = Job::
        where('budgetID', $testBudgetID)->
        where('delete', '=', $helper->reverseCastDelete($delete))->
        get();
        $jobCount = $jobCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/jobs?budgetID='.$testBudgetID.'&delete='.$delete);

        $response->assertJsonCount($jobCount);
        $response->assertStatus(200);
    }  

    /**
     * Test Job index with invalid delete;
     */
    public function testJobIndexInvalidDelete()
    {
        $testBudgetID = Budget::find(5)->id;  
        $helper = new ControllerHelper; 
        $delete = "invalidDelete";
        $standardDelete= DeleteEnum::FALSE;

        $jobCount = Job::
        where('budgetID', $testBudgetID)->
        where('delete', '=', $helper->reverseCastDelete($standardDelete))->
        get();
        $jobCount = $jobCount->slice(OffsetLimitConstant::OFFSET, OffsetLimitConstant::LIMIT)->count(); 
        
        $response = $this->call('GET', '/api/jobs?budgetID='.$testBudgetID.'&delete='.$delete);

        $response->assertJsonCount($jobCount);
        $response->assertStatus(200);
    } 
}
