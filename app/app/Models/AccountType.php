<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Account;

class AccountType extends Model
{
    use HasFactory;
    protected $table = 'account_types';

    protected $primaryKey = 'id';
    protected $fillable = ['name'];


    public function account(){
        return $this->hasMany(Account::class);
    }
}