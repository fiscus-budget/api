<?php

namespace App\Enums;
use App\Enums\Enum;

abstract class SavingTypeEnum extends Enum{
    const ONDATE = 0;
    const ONVALUE = 1;
    const ONGOING = 2;
}
