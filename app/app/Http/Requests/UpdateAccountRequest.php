<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:1|max:30', 
            'value' => 'required|numeric|between:-999999999999999999.99,999999999999999999.99|regex:/^[-]?[0-9]*.[0-9]?[0-9]?$/',
            'typeID' => 'required|integer|exists:account_types,id',
            'budgetID' => 'prohibited'
        ];
    }
}
