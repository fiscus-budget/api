<?php

namespace Tests\Feature\Http\Controllers\Transactions;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Transaction;
use Tests\TestCase;

class TransactionShow extends TestCase
{
    use DatabaseTransactions;
    /**
     * Tests Transaction show.
     *
     */
    public function testTransactionShow()
    {
        $testTransactionID = Transaction::latest('id')->first()->id;
        $response = $this->get('/api/transaction/'.$testTransactionID);

        $response->assertStatus(200);
        $response->assertSessionHasNoErrors();
    }
    
    /**
     * Tests Transaction show method with invalid URL.
     *
     */
    public function testTransactionShowInvalidPath()
    {
        $testTransactionID = Transaction::latest('id')->first()->id;
        $response = $this->post('/api/transactionInvalidPath/'.$testTransactionID);

        $response->assertStatus(404);
    }

    /**
     * Tests Transaction show method with wrong method call.
     *
     */
    public function testTransactionShowInvalidMethod()
    {
        $testTransactionID = Transaction::latest('id')->first()->id;
        $response = $this->post('/api/transaction/'.$testTransactionID);

        $response->assertStatus(405);
    }
}
