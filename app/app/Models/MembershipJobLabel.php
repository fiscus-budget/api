<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MembershipJobLabel extends Model
{
    use HasFactory;

    protected $table = 'membership_job_labels';

    protected $primaryKey = 'id';
    protected $fillable = ['budgetID', 'jobID', 'labelID'];

    public function job(){
        return $this->belongsTo(Job::class, 'jobID');
    }
    
    public function label(){
        return $this->belongsTo(Label::class, 'labelID');
    }

}
